# SteadyContact SDK

SteadyContact is a fully asynchronous C# .NET SDK designed around the Constant Contact API v2

## Installation
Use NuGet to get the latest version of the SDK 

```
Install-Package SteadyContact
```

Create new instance of the SteadyContact object and pass your Api Key and Access Token:

```
#!C#
var settings = new SteadyContactSettings("E63uc1T1LrB3W4P5KUs3yZg59", "ef141a22-b606-4577-b39c-87322f4799fb");
```

Create a service and start calling the API:

```
#!C#
var contactsService = new ContactsService(settings);
var contacts = await contactsService.GetContactsPagedAsync();
```

That's it!

## Documentation

Visit the documentation wiki at: https://bitbucket.org/scottlance/incontact/wiki/Home