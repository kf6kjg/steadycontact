﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.EmailCampaignScheduleService;
using SteadyContactSdk.EmailCampaignScheduleService.Models;
using SteadyContactSdk.EmailCampaignScheduling.Models;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Models;
using SteadyContactSdk.Objects;
using SteadyContactSdk.Validation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SteadyContactSdk.EmailCampaignScheduling
{
    public class EmailCampaignSchedulingService : ServiceBase, IEmailCampaignSchedulingService
    {
        public EmailCampaignSchedulingService(ISteadyContactSettings settings)
            : base(settings)
        { }

        internal EmailCampaignSchedulingService(ISteadyContactSettings settings, IValidationService validationService, IWebServiceRequest webServiceRequest)
            : base(settings, validationService, webServiceRequest)
        { }

        // Reference: http://developer.constantcontact.com/docs/campaign-scheduling/campaign-schedule-collection.html?method=POST
        public async Task<ScheduleResponse> CreateEmailCampaignScheduleAsync(string campaignId, DateTime scheduledDate)
        {
            var model = new CreateEmailCampaignScheduleModel { Settings = settings, CampaignId = campaignId, ScheduledDate = scheduledDate };
            validationService.Validate(model);
            var date = new ScheduledDate { scheduledDate = scheduledDate.ToString("o") };           
            return await webServiceRequest.PostDeserializedAsync<ScheduledDate, ScheduleResponse>(new Uri(model.GenerateEndpoint()), settings.AuthToken, date);
        }

        // Reference: http://developer.constantcontact.com/docs/campaign-scheduling/campaign-schedule-resource.html?method=DELETE
        public async Task DeleteEmailCampaignScheduleAsync(string campaignId, string scheduleId)
        {
            var model = new EmailCampaignScheduleModel { Settings = settings, CampaignId = campaignId, ScheduleId = scheduleId };
            validationService.Validate(model);          
            await webServiceRequest.DeleteAsync(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/campaign-scheduling/campaign-schedule-resource.html?method=GET       
        public async Task<ScheduleResponse> GetEmailCampaignScheduleAsync(string campaignId, string scheduleId)
        {
            var model = new EmailCampaignScheduleModel { Settings = settings, CampaignId = campaignId, ScheduleId = scheduleId };
            validationService.Validate(model);           
            return await webServiceRequest.GetDeserializedAsync<ScheduleResponse>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: https://developer.constantcontact.com/docs/campaign-scheduling/campaign-schedule-collection.html        
        public async Task<IEnumerable<ScheduleResponse>> GetEmailCampaignSchedulesAsync(string campaignId)
        {
            var model = new GetEmailCampaignIdScheduleModel { Settings = settings, CampaignId = campaignId };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<IEnumerable<ScheduleResponse>>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/campaign-scheduling/campaign-schedule-resource.html?method=PUT
        public async Task<ScheduleResponse> UpdateEmailCampaignScheduleAsync(string campaignId, string scheduleId, DateTime scheduledDate)
        {
            var model = new UpdateEmailCampaignScheduleModel { Settings = settings, CampaignId = campaignId, ScheduleId = scheduleId, ScheduledDate = scheduledDate };
            validationService.Validate(model);
            var date = new ScheduledDate { scheduledDate = scheduledDate.ToString("o") };         
            return await webServiceRequest.PutDeserializedAsync<ScheduledDate, ScheduleResponse>(new Uri(model.GenerateEndpoint()), settings.AuthToken, date);
        }
    }
}