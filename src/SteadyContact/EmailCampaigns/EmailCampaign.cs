﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Collections.Generic;
using SteadyContactSdk.EmailCampaigns.Validation;
using SteadyContactSdk.Exceptions;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Validation;
using Newtonsoft.Json;

namespace SteadyContactSdk.EmailCampaigns
{
    [JsonObject]
    public class EmailCampaign : IValidatable
    {
        [JsonProperty("id")]
        public string Id { get; internal set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("subject")]
        public string Subject { get; set; }
        [JsonProperty("status"), JsonConverter(typeof(TypeEnumConverter<EmailCampaignStatus, BiLookup<EmailCampaignStatus, string>>))]
        public EmailCampaignStatus Status { get; internal set; }
        [JsonProperty("from_name")]
        public string FromName { get; set; }
        [JsonProperty("from_email")]
        public string FromEmail { get; set; }
        [JsonProperty("reply_to_email")]
        public string ReplyToEmail { get; set; }
        [JsonProperty("template_type"), JsonConverter(typeof(TypeEnumConverter<TemplateType, BiLookup<TemplateType, string>>))]
        public TemplateType TemplateType { get; set; }
        [JsonProperty("is_permission_reminder_enabled")]
        public bool IsPermissionReminderEnabled { get; set; }
        [JsonProperty("permission_reminder_text")]
        public string PermissionReminderText { get; set; }
        [JsonProperty("is_view_as_webpage_enabled")]
        public bool IsViewAsWebpageEnabled { get; set; }
        [JsonProperty("view_as_web_page_text")]
        public string ViewAsWebPageText { get; set; }
        [JsonProperty("view_as_web_page_link_text")]
        public string ViewAsWebPageLinkText { get; set; }
        [JsonProperty("greeting_salutations")]
        public string GreetingSalutations { get; set; }
        [JsonProperty("greeting_name"), JsonConverter(typeof(TypeEnumConverter<GreetingName, BiLookup<GreetingName, string>>))]
        public GreetingName GreetingName { get; set; }
        [JsonProperty("greeting_string")]
        public string GreetingString { get; set; }
        [JsonProperty("email_content")]
        public string EmailContent { get; set; }
        [JsonProperty("text_content")]
        public string TextContent { get; set; }
        [JsonProperty("email_content_format"), JsonConverter(typeof(TypeEnumConverter<EmailContentFormat, BiLookup<EmailContentFormat, string>>))]
        public EmailContentFormat EmailContentFormat { get; set; }
        [JsonProperty("style_sheet")]
        public string StyleSheet { get; set; }
        [JsonProperty("message_footer")]
        public MessageFooter MessageFooter { get; set; }
        [JsonProperty("permalink_url")]
        public string PermalinkUrl { get; internal set; }
        [JsonProperty("created_date")]
        public string CreatedDate { get; internal set; }
        [JsonProperty("last_run_date")]
        public string LastRunDate { get; internal set; }
        [JsonProperty("next_run_date")]
        public string NextRunDate { get; internal set; }
        [JsonProperty("modified_since")]
        public string ModifiedSince { get; internal set; }
        [JsonProperty("sent_to_contact_lists")]
        public IEnumerable<SentToContactLists> SentToContactLists { get; set; }
        [JsonProperty("tracking_summary")]
        public TrackingSummary TrackingSummary { get; internal set; }
        [JsonProperty("click_through_details")]
        public IEnumerable<ClickThroughDetails> ClickThroughDetails { get; internal set; }

        public IEnumerable<ValidationError> Validate()
        {
            var errors = new List<ValidationError>();
            errors.AddRange(Validator.Validate<EmailCampaign, EmailCampaignValidator>(this));

            if (this.MessageFooter != null)
                errors.AddRange(this.MessageFooter.Validate());

            return errors;
        }
    }
}