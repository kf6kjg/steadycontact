﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;
using SteadyContactSdk.Helpers;

namespace SteadyContactSdk.EmailCampaigns.Validation
{
    internal class EmailCampaignValidator : AbstractValidator<EmailCampaign>
    {
        public EmailCampaignValidator()
        {
            RuleFor(e => e.EmailContent).NotEmpty().WithMessage("EmailContent is required").Length(0, 930000).WithMessage("EmailContent must not exceed 930,000 characters");
            RuleFor(e => e.EmailContent).Null().When(e => e.TemplateType != TemplateType.None && (e.TemplateType == TemplateType.Stock || e.TemplateType == TemplateType.TemplateV2)).WithMessage("EmailContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2");
            RuleFor(e => e.EmailContentFormat).Must(e => e == EmailContentFormat.None).When(e => e.TemplateType == TemplateType.TemplateV2).WithMessage("EmailContentFormat cannot be set when TemplateType is set to TEMPLATE_V2");
            RuleFor(e => e.FromEmail).NotEmpty().WithMessage("FromEmail is required").Length(0, 80).WithMessage("FromEmail must not exceed 80 characters");
            RuleFor(e => e.FromName).NotEmpty().WithMessage("FromName is required").Length(0, 100).WithMessage("FromName must not exceed 100 characters");
            RuleFor(e => e.GreetingSalutations).Length(0, 50).WithMessage("GreetingSalutations must not exceed 50 characters");
            RuleFor(e => e.GreetingString).Length(0, 1500).WithMessage("GreetingString must not exceed 1,500 characters");
            RuleFor(e => e.Name).NotEmpty().WithMessage("Name is required").Length(0, 80).WithMessage("Name must not exceed 80 characters");
            RuleFor(e => e.PermissionReminderText).Length(0, 1500).WithMessage("PermissionReminderText must not exceed 1,500 characters");
            RuleFor(e => e.PermissionReminderText).NotEmpty().When(e => e.IsPermissionReminderEnabled).WithMessage("PermissionReminderText is required when IsPermissionReminderEnabled is true");
            RuleFor(e => e.ReplyToEmail).NotEmpty().WithMessage("ReplyToEmail is required").Length(0, 80).WithMessage("ReplyToEmail must not exceed 80 characters");
            RuleFor(e => e.StyleSheet).Empty().When(e => e.EmailContentFormat == EmailContentFormat.Html).WithMessage("StyleSheet must be empty if the EmailContentFormat is not XHTML");
            RuleFor(e => e.StyleSheet).Null().When(e => e.TemplateType != TemplateType.None && (e.TemplateType == TemplateType.TemplateV2)).WithMessage("StyleSheet cannot be set when TemplateType is set to TEMPLATE_V2");
            RuleFor(e => e.Subject).NotEmpty().WithMessage("Subject is required").Length(0, 200).WithMessage("Subject must not exceed 200 characters");
            RuleFor(e => e.TextContent).NotEmpty().WithMessage("TextContent is required").Length(0, 930000).WithMessage("TextContent must not exceed 930,000 characters");
            RuleFor(e => e.TextContent).Must((string tc) => tc != null && (tc.StartsWith("<text>") && tc.EndsWith("</text>"))).When(e => e.EmailContentFormat != EmailContentFormat.None && e.EmailContentFormat == EmailContentFormat.Xhtml).WithMessage("TextContent must be wrapped in a <text> tag when EmailContentFormat is XHTML");
            RuleFor(e => e.TextContent).Null().When(e => e.TemplateType != TemplateType.None && (e.TemplateType == TemplateType.Stock || e.TemplateType == TemplateType.TemplateV2)).WithMessage("TextContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2");
            RuleFor(e => e.ViewAsWebPageLinkText).Length(0, 50).WithMessage("ViewAsWebPageLinkText must not exceed 50 characters");
            RuleFor(e => e.ViewAsWebPageLinkText).NotNull().NotEmpty().When(e => e.IsViewAsWebpageEnabled).WithMessage("ViewAsWebPageLinkText is required when IsViewAsWebpageEnabled is set to true");
            RuleFor(e => e.ViewAsWebPageText).Length(0, 50).WithMessage("ViewAsWebPageText must not exceed 50 characters");
            RuleFor(e => e.ViewAsWebPageText).NotNull().NotEmpty().When(e => e.IsViewAsWebpageEnabled).WithMessage("ViewAsWebPageText is required when IsViewAsWebpageEnabled is set to true");
        }
    }
}