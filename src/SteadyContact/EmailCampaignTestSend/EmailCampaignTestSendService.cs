﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SteadyContactSdk.EmailCampaignTestSend.Models;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Models;
using SteadyContactSdk.Validation;

namespace SteadyContactSdk.EmailCampaignTestSend
{
    public class EmailCampaignTestSendService : ServiceBase, IEmailCampaignTestSendService
    {
        public EmailCampaignTestSendService(ISteadyContactSettings settings)
            : base(settings)
        { }

        internal EmailCampaignTestSendService(ISteadyContactSettings settings, IValidationService validationService, IWebServiceRequest webServiceRequest)
            : base(settings, validationService, webServiceRequest)
        { }

        // Reference: http://developer.constantcontact.com/docs/email-testsend/email-campaign-testsend-api.html?method=POST
        public async Task SendEmailCampaignTestAsync(string campaignId, IEnumerable<string> testRecipients, EmailFormat messageFormat, string personalMessage)
        {
            var model = new SendEmailCampaignTestModel() { CampaignId = campaignId, EmailAddresses = testRecipients, MessageFormat = messageFormat, PersonalMessage = personalMessage };
            validationService.Validate(model);

            await webServiceRequest.PostAsync<SendEmailCampaignTestModel>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, model);
        }

        // Reference: http://developer.constantcontact.com/docs/email-testsend/email-campaign-preview.html?method=GET
        public async Task<GetEmailCampaignPreviewModel> GetEmailCampaignPreviewAsync(string campaignId)
        {
            var model = new GetEmailCampaignPreviewModel() { CampaignId = campaignId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<GetEmailCampaignPreviewModel>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }
    }
}