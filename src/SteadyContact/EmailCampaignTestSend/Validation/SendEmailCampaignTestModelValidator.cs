﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Linq;
using FluentValidation;
using SteadyContactSdk.EmailCampaignTestSend.Models;

namespace SteadyContactSdk.EmailCampaignTestSend.Validation
{
    internal class SendEmailCampaignTestModelValidator : AbstractValidator<SendEmailCampaignTestModel>
    {
        public SendEmailCampaignTestModelValidator()
        {
            RuleFor(m => m.CampaignId).NotEmpty().WithMessage("CampaignId is required");
            RuleFor(m => m.EmailAddresses).NotEmptyList().WithMessage("Email address list must contain at least one entry");
            RuleFor(m => m.EmailAddresses).Must(ea => ea.Count() <= 5).When(a => a.EmailAddresses != null).WithMessage("You cannot specify more than 5 email addresses");
            RuleFor(m => m.MessageFormatAsString).Must(mf => mf == "HTML" || mf == "TEXT" || mf == "HTML_AND_TEXT").WithMessage("MessageFormat must be one of the following values: HTML, TEXT or HTML_AND_TEXT");
            RuleFor(o => o.PersonalMessage).Length(0, 5000).WithMessage("Personal Message cannot contain more than 5000 characters");
        }
    }
}