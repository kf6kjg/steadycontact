﻿using System.Collections;
using System.Collections.Generic;

namespace SteadyContactSdk.Helpers
{
    // Thanks Jon Skeet - http://stackoverflow.com/questions/255341/getting-key-of-value-of-a-generic-dictionary
    public class BiLookup<TFirst, TSecond> : IEnumerable<KeyValuePair<TFirst, TSecond>>
    {
        IDictionary<TFirst, TSecond> firstToSecond = new Dictionary<TFirst, TSecond>();
        IDictionary<TSecond, TFirst> secondToFirst = new Dictionary<TSecond, TFirst>();

        private static TFirst EmptyFirst = default(TFirst);
        private static TSecond EmptySecond = default(TSecond);

        public void Add(TFirst first, TSecond second)
        {
            TFirst firsts;
            TSecond seconds;
            if (!firstToSecond.TryGetValue(first, out seconds))
            {
                firstToSecond[first] = second;
            }
            if (!secondToFirst.TryGetValue(second, out firsts))
            {
                secondToFirst[second] = first;
            }
        }

        // Note potential ambiguity using indexers (e.g. mapping from int to int)
        // Hence the methods as well...
        public TSecond this[TFirst first]
        {
            get { return GetByFirst(first); }
        }

        public TFirst this[TSecond second]
        {
            get { return GetBySecond(second); }
        }

        public TSecond GetByFirst(TFirst first)
        {
            TSecond value;
            if (!firstToSecond.TryGetValue(first, out value))
            {
                return EmptySecond;
            }
            return value; // Create a copy for sanity
        }

        public TFirst GetBySecond(TSecond second)
        {
            TFirst value;
            if (!secondToFirst.TryGetValue(second, out value))
            {
                return EmptyFirst;
            }
            return value; // Create a copy for sanity
        }

        #region IEnumerable
        public IEnumerator<KeyValuePair<TFirst, TSecond>> GetEnumerator()
        {
            return firstToSecond.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}