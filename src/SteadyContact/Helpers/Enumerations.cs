﻿#region License
// The MIT License (MIT)

//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

namespace SteadyContactSdk.Helpers
{
    public enum ActionBy { None, ActionByOwner, ActionByVisitor }
    public enum BulkActivitySortBy { DateDesc, EmailAddress }
    public enum BulkActivityStatus { None, All, Complete, Error, Pending, Queued, Running, Unconfirmed }
    public enum BulkActivityType { None, AddContacts, ClearContactsFromLists, ExportContacts, RemoveContactsFromLists }
    public enum BulkExportFileFormat { Csv, Txt, Xls, Xlxs }
    public enum BulkUploadFileFormat { Csv, Txt, Xls, Xlxs }
    public enum ContactAddressType { None, Business, Personal }
    public enum ContactListStatus { Active, Hidden }
    public enum ContactListsStatus { Active, Hidden, Removed }
    public enum ContactStatus { Active, NonSubscriber, OptOut, Removed, TempHold, Unconfirmed }
    public enum ContactStatusQuery { All, Active, OptOut, Removed, Unconfirmed }
    public enum CountryCode
    {
        NONE,
        AD,
        AE,
        AF,
        AG,
        AI,
        AL,
        AM,
        AO,
        AQ,
        AR,
        AS,
        AT,
        AU,
        AW,
        AX,
        AZ,
        BA,
        BB,
        BD,
        BE,
        BF,
        BG,
        BH,
        BI,
        BJ,
        BL,
        BM,
        BN,
        BO,
        BQ,
        BR,
        BS,
        BT,
        BV,
        BW,
        BY,
        BZ,
        CA,
        CC,
        CD,
        CF,
        CG,
        CH,
        CI,
        CK,
        CL,
        CM,
        CN,
        CO,
        CR,
        CU,
        CV,
        CW,
        CX,
        CY,
        CZ,
        DJ,
        DK,
        DM,
        DO,
        DZ,
        EC,
        EE,
        EG,
        EH,
        ER,
        ES,
        ET,
        FI,
        FJ,
        FK,
        FM,
        FO,
        FR,
        GA,
        GD,
        GE,
        GF,
        GG,
        GH,
        GI,
        GL,
        GM,
        GN,
        GP,
        GQ,
        GR,
        GS,
        GT,
        GU,
        GW,
        GY,
        HK,
        HM,
        HN,
        HR,
        HT,
        HU,
        ID,
        IE,
        IL,
        IM,
        IN,
        IO,
        IQ,
        IR,
        IS,
        IT,
        JE,
        JM,
        JO,
        JP,
        KE,
        KG,
        KH,
        KI,
        KM,
        KN,
        KP,
        KR,
        KW,
        KY,
        KZ,
        LA,
        LB,
        LC,
        LI,
        LK,
        LR,
        LS,
        LT,
        LU,
        LV,
        LY,
        MA,
        MC,
        MD,
        ME,
        MF,
        MG,
        MH,
        MK,
        ML,
        MM,
        MN,
        MO,
        MP,
        MQ,
        MR,
        MS,
        MT,
        MU,
        MV,
        MW,
        MX,
        MY,
        MZ,
        NA,
        NC,
        NE,
        NF,
        NG,
        NI,
        NL,
        NO,
        NP,
        NR,
        NU,
        NZ,
        OM,
        PA,
        PE,
        PF,
        PG,
        PH,
        PK,
        PL,
        PM,
        PN,
        PR,
        PS,
        PT,
        PW,
        PY,
        QA,
        RE,
        RO,
        RS,
        RU,
        RW,
        SA,
        SB,
        SC,
        SD,
        SE,
        SG,
        SH,
        SI,
        SJ,
        SK,
        SL,
        SM,
        SN,
        SO,
        SR,
        SS,
        ST,
        SV,
        SX,
        SY,
        SZ,
        TC,
        TD,
        TF,
        TG,
        TH,
        TJ,
        TK,
        TL,
        TM,
        TN,
        TO,
        TR,
        TT,
        TV,
        TW,
        TZ,
        UA,
        UG,
        UM,
        US,
        UY,
        UZ,
        VA,
        VC,
        VE,
        VG,
        VI,
        VN,
        VU,
        WF,
        WS,
        YE,
        YT,
        ZA,
        ZM,
        ZW
    }
    public enum CurrencyType { AUD, CAD, CHF, CZK, DKK, EUR, GBP, HKD, HUF, ILS, JPY, MXN, NOK, NONE, NZD, PHP, PLN, SEK, SGD, THB, TWD, USD }
    public enum CustomFieldValue
    {
        CustomField1,
        CustomField2,
        CustomField3,
        CustomField4,
        CustomField5,
        CustomField6,
        CustomField7,
        CustomField8,
        CustomField9,
        CustomField10,
        CustomField11,
        CustomField12,
        CustomField13,
        CustomField14,
        CustomField15
    }
    public enum EmailAddressConfirmStatus { Confirmed, NoConfirmationRequired, Unconfirmed }
    public enum EmailAddressStatus { None, All, Confirmed, Unconfirmed }
    public enum EmailCampaignStatus { None, All, Draft, Running, Sent, Scheduled }
    public enum EmailContentFormat { None, Html, Xhtml }
    public enum EmailFormat { Html, Text, HtmlAndText }
    public enum EventSpotFeeScope { Both, Guests, Registrants }
    public enum EventSpotInformationSection { Contact, Location, Time }
    public enum EventSpotStatus { Active, Cancelled, Complete, Deleted, Draft }
    public enum EventSpotType
    {
        Auction,
        Birthday,
        BusinessFinanceSales,
        ClassesWorkshops,
        CompetitionSports,
        ConferencesSeminarsForum,
        ConventionsTradeshowsExpos,
        FestivalsFairs,
        FoodWine,
        FundraisersCharities,
        Holiday,
        IncentiveRewardRecognition,
        MoviesFilm,
        MusicConcerts,
        NetworkingClubs,
        PerformingArts,
        Other,
        OutdoorsRecreation,
        ReligionSpirituality,
        SchoolsReunionsAlumni,
        PartiesSocialEventsMixers,
        Travel,
        WebinarTeleseminarTeleclass,
        Weddings
    }
    public enum ExportColumnName
    {
        AddressLine1,
        AddressLine2,
        AddressLine3,
        City,
        CompanyName,
        Country,
        CustomField1,
        CustomField2,
        CustomField3,
        CustomField4,
        CustomField5,
        CustomField6,
        CustomField7,
        CustomField8,
        CustomField9,
        CustomField10,
        CustomField11,
        CustomField12,
        CustomField13,
        CustomField14,
        CustomField15,
        Email,
        FirstName,
        HomePhone,
        JobTitle,
        LastName,
        State,
        USStateCAProvince,
        WorkPhone,
        ZipPostalCode
    }
    public enum GreetingName { None, FirstName, LastName, FirstAndLastName }
    public enum ImportColumnName
    {
        AddressLine1,
        AddressLine2,
        AddressLine3,
        Anniversary,
        BirthdayDay,
        BirthdayMonth,
        City,
        CompanyName,
        Country,
        CustomField1,
        CustomField2,
        CustomField3,
        CustomField4,
        CustomField5,
        CustomField6,
        CustomField7,
        CustomField8,
        CustomField9,
        CustomField10,
        CustomField11,
        CustomField12,
        CustomField13,
        CustomField14,
        CustomField15,
        Email,
        FirstName,
        HomePhone,
        JobTitle,
        LastName,
        State,
        WorkPhone,
        ZipPostalCode
    }
    public enum LibrarySortBy { CreatedDateDesc, CreatedDate, ModifiedDate, ModifiedDateDesc, Name, NameDesc, Size, SizeDesc, Dimension, DimensionDesc }
    public enum LibrarySource { All, MyComputer, Facebook, Instagram, Shutterstock, Mobile }
    public enum LibraryType { All, Images, Documents }
    public enum LibraryFileType { Jpeg, Jpg, Gif, Pdf, Png, Doc, Docx, Xls, Xlsx, Ppt, Pptx }
    public enum LibraryStatus { Active, Processing, Uploaded, VirusFound, Failed, Deleted }
    public enum OptInSource { None, ActionByOwner, ActionBySystem, ActionByVisitor }
    public enum OptOutSource { None, ActionByOwner, ActionByVisitor }
    public enum PaymentType { Check, Door, GoogleCheckout, PayPal }
    public enum PromoCodeDiscountScope { None, FeeList, OrderTotal }
    public enum PromoCodeType { Access, Discount }
    public enum SdkBoolean { True, False }
    public enum ServiceEndpoint
    {
        Activities,
        AccountInfo,
        AddContacts,
        Bounces,
        ClearLists,
        Clicks,
        ContactList,
        Contacts,
        EmailCampaigns,
        EventSpotEvents,
        ExportContacts,
        ExportFiles,
        Forwards,
        Library,
        LibraryFiles,
        LibraryFilesUploadStatus,
        LibraryFolders,
        LibraryTrashFolder,
        Opens,
        RemoveFromLists,
        Sends,
        Summary,
        SummaryByCampaign,
        Tracking,
        Unsubscribes,
        VerifiedEmailAddress
    }
    public enum TemplateType { None, Custom, Stock, TemplateV2 }
    public enum TimeZone
    {
        Africa_Accra,
        Africa_Casablanca,
        Africa_Harare,
        Africa_Johannesburg,
        Africa_Lagos,
        Africa_Monrovia,
        Africa_Nairobi,
        Africa_Windhoek,
        America_Asuncion,
        America_Bogota,
        America_Buenos_Aires,
        America_Caracas,
        America_Cayenne,
        America_Chihuahua,
        America_Cuiaba,
        America_Fortaleza,
        America_Godthab,
        America_Guayaquil,
        America_Guyana,
        America_La_Paz,
        America_Lima,
        America_Manaus,
        America_Mexico_City,
        America_Montevideo,
        America_Paramaribo,
        America_Puerto_Rico,
        America_Santiago,
        Asia_Almaty,
        Asia_Amman,
        Asia_Aqtob,
        Asia_Ashgabat,
        Asia_Baghdad,
        Asia_Baku,
        Asia_Bangkok,
        Asia_Beirut,
        Asia_Bishkek,
        Asia_Brunei,
        Asia_Chongqing,
        Asia_Dhaka,
        Asia_Dili,
        Asia_Dubai,
        Asia_Dushanbe,
        Asia_Hong_Kong,
        Asia_Irkutsk,
        Asia_Jakarta,
        Asia_Jayapura,
        Asia_Jerusalem,
        Asia_Kabul,
        Asia_Kamchatka,
        Asia_Karachi,
        Asia_Kathmandu,
        Asia_Kolkata,
        Asia_Krasnoyarsk,
        Asia_Kuala_Lumpur,
        Asia_Kuwait,
        Asia_Magadan,
        Asia_Makassar,
        Asia_Manila,
        Asia_Novosibirsk,
        Asia_Omsk,
        Asia_Oral,
        Asia_Qyzylorda,
        Asia_Rangoon,
        Asia_Seoul,
        Asia_Singapore,
        Asia_Taipei,
        Asia_Tashkent,
        Asia_Tbilisi,
        Asia_Tehran,
        Asia_Thimbu,
        Asia_Tokyo,
        Asia_Ulaanbaatar,
        Asia_Vladivostok,
        Asia_Yakutsk,
        Asia_Yekaterinburg,
        Asia_Yerevan,
        Atlantic_Azores,
        Atlantic_Cape_Verde,
        Atlantic_South_Georgia,
        Australia_Adelaide,
        Australia_Brisbane,
        Australia_Canberra,
        Australia_Darwin,
        Australia_Hobart,
        Australia_Perth,
        Brazil_East,
        CST,
        Canada_Atlantic,
        Canada_Newfoundland,
        Canada_Saskatchewan,
        Etc_Gtm_Plus_12,
        Etc_Universal,
        Europe_Amsterdam,
        Europe_Athens,
        Europe_Belgrade,
        Europe_Brussels,
        Europe_Helsinki,
        Europe_Lisbon,
        Europe_London,
        Europe_Minsk,
        Europe_Moscow,
        Europe_Sarajevo,
        Europe_Volgograd,
        Indian_Mahe,
        Indian_Maldives,
        Indian_Mauritius,
        Indian_Reunion,
        Mexico_BajaNorte,
        Pacific_Auckland,
        Pacific_Efate,
        Pacific_Fiji,
        Pacific_Guadalcanal,
        Pacific_Guam,
        Pacific_Noumea,
        Pacific_Port_Moresby,
        Pacific_Samoa,
        Pacific_Tahiti,
        Pacific_Tongatapu,
        US_Alaska,
        US_Arizona,
        US_Central,
        US_East_Indiana,
        US_Eastern,
        US_Hawaii,
        US_Mountain,
        US_Pacific
    }

    public static class EnumStrings
    {
        public static object GetEnumMappings<T>()
        {
            switch (typeof(T).Name)
            {
                case "ActionBy": return ActionByStrings;
                case "BulkActivitySortBy": return BulkActivitySortByStrings;
                case "BulkActivityStatus": return BulkActivityStatusStrings;
                case "BulkActivityType": return BulkActivityTypeStrings;
                case "BulkExportFileFormat": return BulkExportFileFormatStrings;
                case "BulkUploadFileFormat": return BulkUploadFileFormatStrings;
                case "ContactAddressType": return ContactAddressTypeStrings;
                case "ContactListStatus": return ContactListStatusStrings;
                case "ContactListsStatus": return ContactListsStatusStrings;
                case "ContactStatus": return ContactStatusStrings;
                case "ContactStatusQuery": return ContactStatusQueryStrings;
                case "CountryCode": return CountryCodeStrings;
                case "CurrencyType": return CurrencyTypeStrings;
                case "CustomFieldValue": return CustomFieldValueStrings;
                case "EmailAddressConfirmStatus": return EmailAddressConfirmStatusStrings;
                case "EmailAddressStatus": return EmailAddressStatusStrings;
                case "EmailCampaignStatus": return EmailCampaignStatusStrings;
                case "EmailContentFormat": return EmailContentFormatStrings;
                case "EmailFormat": return EmailFormatStrings;
                case "EventSpotFeeScope": return EventSpotFeeScopeStrings;
                case "EventSpotInformationSection": return EventSpotInformationSectionStrings;
                case "EventSpotStatus": return EventSpotStatusStrings;
                case "EventSpotType": return EventSpotTypeStrings;
                case "ExportColumnName": return ExportColumnNameStrings;
                case "GreetingName": return GreetingNameStrings;
                case "ImportColumnName": return ImportColumnNameStrings;
                case "LibraryFileType": return LibraryFileTypeStrings;
                case "LibrarySortBy": return LibrarySortByStrings;
                case "LibrarySource": return LibrarySourceStrings;
                case "LibraryStatus": return LibraryStatusStrings;
                case "LibraryType": return LibraryTypeStrings;
                case "OptInSource": return OptInSourceStrings;
                case "OptOutSource": return OptOutSourceStrings;
                case "PaymentType": return PaymentTypeStrings;
                case "PromoCodeDiscountScope": return PromoCodeDiscountScopeStrings;
                case "PromoCodeType": return PromoCodeTypeStrings;
                case "ServiceEndpoint": return ServiceEndpointStrings;
                case "SdkBoolean": return SdkBooleanStrings;
                case "TemplateType": return TemplateTypeStrings;
                case "TimeZone": return TimeZoneStrings;
                default: return null;
            }
        }

        public static BiLookup<ActionBy, string> ActionByStrings = new BiLookup<ActionBy, string>
        {
            { ActionBy.None, "" },
            { ActionBy.ActionByOwner, "ACTION_BY_OWNER" },
            { ActionBy.ActionByVisitor, "ACTION_BY_VISITOR" }
        };
        public static BiLookup<BulkActivitySortBy, string> BulkActivitySortByStrings = new BiLookup<BulkActivitySortBy, string>
        {
            { BulkActivitySortBy.DateDesc, "DATE_DESC" },
            { BulkActivitySortBy.EmailAddress, "EMAIL_ADDRESS" }
        };
        public static BiLookup<BulkActivityStatus, string> BulkActivityStatusStrings = new BiLookup<BulkActivityStatus, string>
        {
            { BulkActivityStatus.None, "" },
            { BulkActivityStatus.All, "ALL" },
            { BulkActivityStatus.Complete, "COMPLETE" },
            { BulkActivityStatus.Error, "ERROR" },
            { BulkActivityStatus.Pending, "PENDING" },
            { BulkActivityStatus.Queued, "QUEUED" },
            { BulkActivityStatus.Running, "RUNNING" },
            { BulkActivityStatus.Unconfirmed, "UNCONFIRMED" }
        };
        public static BiLookup<BulkActivityType, string> BulkActivityTypeStrings = new BiLookup<BulkActivityType, string>
        {
            { BulkActivityType.None, "" },
            { BulkActivityType.AddContacts, "ADD_CONTACTS" },
            { BulkActivityType.ClearContactsFromLists, "CLEAR_CONTACTS_FROM_LISTS" },
            { BulkActivityType.ExportContacts, "EXPORT_CONTACTS" },
            { BulkActivityType.RemoveContactsFromLists, "REMOVE_CONTACTS_FROM_LISTS" }
        };
        public static BiLookup<BulkExportFileFormat, string> BulkExportFileFormatStrings = new BiLookup<BulkExportFileFormat, string>
        {
            { BulkExportFileFormat.Csv, "csv" },
            { BulkExportFileFormat.Txt, "txt" },
            { BulkExportFileFormat.Xls, "xls" },
            { BulkExportFileFormat.Xlxs, "xlxs" }
        };
        public static BiLookup<BulkUploadFileFormat, string> BulkUploadFileFormatStrings = new BiLookup<BulkUploadFileFormat, string>
        {
            { BulkUploadFileFormat.Csv, "text/csv" },
            { BulkUploadFileFormat.Txt, "text/plain" },
            { BulkUploadFileFormat.Xls, "application/vnd.ms-excel" },
            { BulkUploadFileFormat.Xlxs, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }
        };
        public static BiLookup<ContactAddressType, string> ContactAddressTypeStrings = new BiLookup<ContactAddressType, string>
        {
            { ContactAddressType.None, "NONE" },
            { ContactAddressType.Business, "BUSINESS" },
            { ContactAddressType.Personal, "PERSONAL" },
        };
        public static BiLookup<ContactListStatus, string> ContactListStatusStrings = new BiLookup<ContactListStatus, string>
        {
            { ContactListStatus.Active, "ACTIVE" },
            { ContactListStatus.Hidden, "HIDDEN" }           
        };
        public static BiLookup<ContactListsStatus, string> ContactListsStatusStrings = new BiLookup<ContactListsStatus, string>
        {
            { ContactListsStatus.Active, "ACTIVE" },
            { ContactListsStatus.Hidden, "HIDDEN" },
            { ContactListsStatus.Removed, "REMOVED" }
        };
        public static BiLookup<ContactStatus, string> ContactStatusStrings = new BiLookup<ContactStatus, string>
        {
            { ContactStatus.Active, "ACTIVE" },
            { ContactStatus.NonSubscriber, "NON_SUBSCRIBER" },
            { ContactStatus.OptOut, "OPTOUT" },
            { ContactStatus.Removed, "REMOVED" },
            { ContactStatus.TempHold, "TEMP_HOLD" },
            { ContactStatus.Unconfirmed, "UNCONFIRMED" },
        };
        public static BiLookup<ContactStatusQuery, string> ContactStatusQueryStrings = new BiLookup<ContactStatusQuery, string>
        {
            { ContactStatusQuery.All, "ALL" },
            { ContactStatusQuery.Active, "ACTIVE" },
            { ContactStatusQuery.OptOut, "OPTOUT" }, 
            { ContactStatusQuery.Removed, "REMOVED" }, 
            { ContactStatusQuery.Unconfirmed, "UNCONFIRMED" }
        };
        public static BiLookup<CountryCode, string> CountryCodeStrings = new BiLookup<CountryCode, string>
        {
            { CountryCode.AD, "AD" },
            { CountryCode.AE, "AE" },
            { CountryCode.AF, "AF" },
            { CountryCode.AG, "AG" },
            { CountryCode.AI, "AI" },
            { CountryCode.AL, "AL" },
            { CountryCode.AM, "AM" },
            { CountryCode.AO, "AO" },
            { CountryCode.AQ, "AQ" },
            { CountryCode.AR, "AR" },
            { CountryCode.AS, "AS" },
            { CountryCode.AT, "AT" },
            { CountryCode.AU, "AU" },
            { CountryCode.AW, "AW" },
            { CountryCode.AX, "AX" },
            { CountryCode.AZ, "AZ" },
            { CountryCode.BA, "BA" },
            { CountryCode.BB, "BB" },
            { CountryCode.BD, "BD" },
            { CountryCode.BE, "BE" },
            { CountryCode.BF, "BF" },
            { CountryCode.BG, "BG" },
            { CountryCode.BH, "BH" },
            { CountryCode.BI, "BI" },
            { CountryCode.BJ, "BJ" },
            { CountryCode.BL, "BL" },
            { CountryCode.BM, "BM" },
            { CountryCode.BN, "BN" },
            { CountryCode.BO, "BO" },
            { CountryCode.BQ, "BQ" },
            { CountryCode.BR, "BR" },
            { CountryCode.BS, "BS" },
            { CountryCode.BT, "BT" },
            { CountryCode.BV, "BV" },
            { CountryCode.BW, "BW" },
            { CountryCode.BY, "BY" },
            { CountryCode.BZ, "BZ" },
            { CountryCode.CA, "CA" },
            { CountryCode.CC, "CC" },
            { CountryCode.CD, "CD" },
            { CountryCode.CF, "CF" },
            { CountryCode.CG, "CG" },
            { CountryCode.CH, "CH" },
            { CountryCode.CI, "CI" },
            { CountryCode.CK, "CK" },
            { CountryCode.CL, "CL" },
            { CountryCode.CM, "CM" },
            { CountryCode.CN, "CN" },
            { CountryCode.CO, "CO" },
            { CountryCode.CR, "CR" },
            { CountryCode.CU, "CU" },
            { CountryCode.CV, "CV" },
            { CountryCode.CW, "CW" },
            { CountryCode.CX, "CX" },
            { CountryCode.CY, "CY" },
            { CountryCode.CZ, "CZ" },
            { CountryCode.DJ, "DJ" },
            { CountryCode.DK, "DK" },
            { CountryCode.DM, "DM" },
            { CountryCode.DO, "DO" },
            { CountryCode.DZ, "DZ" },
            { CountryCode.EC, "EC" },
            { CountryCode.EE, "EE" },
            { CountryCode.EG, "EG" },
            { CountryCode.EH, "EH" },
            { CountryCode.ER, "ER" },
            { CountryCode.ES, "ES" },
            { CountryCode.ET, "ET" },
            { CountryCode.FI, "FI" },
            { CountryCode.FJ, "FJ" },
            { CountryCode.FK, "FK" },
            { CountryCode.FM, "FM" },
            { CountryCode.FO, "FO" },
            { CountryCode.FR, "FR" },
            { CountryCode.GA, "GA" },
            { CountryCode.GD, "GD" },
            { CountryCode.GE, "GE" },
            { CountryCode.GF, "GF" },
            { CountryCode.GG, "GG" },
            { CountryCode.GH, "GH" },
            { CountryCode.GI, "GI" },
            { CountryCode.GL, "GL" },
            { CountryCode.GM, "GM" },
            { CountryCode.GN, "GN" },
            { CountryCode.GP, "GP" },
            { CountryCode.GQ, "GQ" },
            { CountryCode.GR, "GR" },
            { CountryCode.GS, "GS" },
            { CountryCode.GT, "GT" },
            { CountryCode.GU, "GU" },
            { CountryCode.GW, "GW" },
            { CountryCode.GY, "GY" },
            { CountryCode.HK, "HK" },
            { CountryCode.HM, "HM" },
            { CountryCode.HN, "HN" },
            { CountryCode.HR, "HR" },
            { CountryCode.HT, "HT" },
            { CountryCode.HU, "HU" },
            { CountryCode.ID, "ID" },
            { CountryCode.IE, "IE" },
            { CountryCode.IL, "IL" },
            { CountryCode.IM, "IM" },
            { CountryCode.IN, "IN" },
            { CountryCode.IO, "IO" },
            { CountryCode.IQ, "IQ" },
            { CountryCode.IR, "IR" },
            { CountryCode.IS, "IS" },
            { CountryCode.IT, "IT" },
            { CountryCode.JE, "JE" },
            { CountryCode.JM, "JM" },
            { CountryCode.JO, "JO" },
            { CountryCode.JP, "JP" },
            { CountryCode.KE, "KE" },
            { CountryCode.KG, "KG" },
            { CountryCode.KH, "KH" },
            { CountryCode.KI, "KI" },
            { CountryCode.KM, "KM" },
            { CountryCode.KN, "KN" },
            { CountryCode.KP, "KP" },
            { CountryCode.KR, "KR" },
            { CountryCode.KW, "KW" },
            { CountryCode.KY, "KY" },
            { CountryCode.KZ, "KZ" },
            { CountryCode.LA, "LA" },
            { CountryCode.LB, "LB" },
            { CountryCode.LC, "LC" },
            { CountryCode.LI, "LI" },
            { CountryCode.LK, "LK" },
            { CountryCode.LR, "LR" },
            { CountryCode.LS, "LS" },
            { CountryCode.LT, "LT" },
            { CountryCode.LU, "LU" },
            { CountryCode.LV, "LV" },
            { CountryCode.LY, "LY" },
            { CountryCode.MA, "MA" },
            { CountryCode.MC, "MC" },
            { CountryCode.MD, "MD" },
            { CountryCode.ME, "ME" },
            { CountryCode.MF, "MF" },
            { CountryCode.MG, "MG" },
            { CountryCode.MH, "MH" },
            { CountryCode.MK, "MK" },
            { CountryCode.ML, "ML" },
            { CountryCode.MM, "MM" },
            { CountryCode.MN, "MN" },
            { CountryCode.MO, "MO" },
            { CountryCode.MP, "MP" },
            { CountryCode.MQ, "MQ" },
            { CountryCode.MR, "MR" },
            { CountryCode.MS, "MS" },
            { CountryCode.MT, "MT" },
            { CountryCode.MU, "MU" },
            { CountryCode.MV, "MV" },
            { CountryCode.MW, "MW" },
            { CountryCode.MX, "MX" },
            { CountryCode.MY, "MY" },
            { CountryCode.MZ, "MZ" },
            { CountryCode.NA, "NA" },
            { CountryCode.NC, "NC" },
            { CountryCode.NE, "NE" },
            { CountryCode.NF, "NF" },
            { CountryCode.NG, "NG" },
            { CountryCode.NI, "NI" },
            { CountryCode.NL, "NL" },
            { CountryCode.NO, "NO" },
            { CountryCode.NONE, "" },
            { CountryCode.NP, "NP" },
            { CountryCode.NR, "NR" },
            { CountryCode.NU, "NU" },
            { CountryCode.NZ, "NZ" },
            { CountryCode.OM, "OM" },
            { CountryCode.PA, "PA" },
            { CountryCode.PE, "PE" },
            { CountryCode.PF, "PF" },
            { CountryCode.PG, "PG" },
            { CountryCode.PH, "PH" },
            { CountryCode.PK, "PK" },
            { CountryCode.PL, "PL" },
            { CountryCode.PM, "PM" },
            { CountryCode.PN, "PN" },
            { CountryCode.PR, "PR" },
            { CountryCode.PS, "PS" },
            { CountryCode.PT, "PT" },
            { CountryCode.PW, "PW" },
            { CountryCode.PY, "PY" },
            { CountryCode.QA, "QA" },
            { CountryCode.RE, "RE" },
            { CountryCode.RO, "RO" },
            { CountryCode.RS, "RS" },
            { CountryCode.RU, "RU" },
            { CountryCode.RW, "RW" },
            { CountryCode.SA, "SA" },
            { CountryCode.SB, "SB" },
            { CountryCode.SC, "SC" },
            { CountryCode.SD, "SD" },
            { CountryCode.SE, "SE" },
            { CountryCode.SG, "SG" },
            { CountryCode.SH, "SH" },
            { CountryCode.SI, "SI" },
            { CountryCode.SJ, "SJ" },
            { CountryCode.SK, "SK" },
            { CountryCode.SL, "SL" },
            { CountryCode.SM, "SM" },
            { CountryCode.SN, "SN" },
            { CountryCode.SO, "SO" },
            { CountryCode.SR, "SR" },
            { CountryCode.SS, "SS" },
            { CountryCode.ST, "ST" },
            { CountryCode.SV, "SV" },
            { CountryCode.SX, "SX" },
            { CountryCode.SY, "SY" },
            { CountryCode.SZ, "SZ" },
            { CountryCode.TC, "TC" },
            { CountryCode.TD, "TD" },
            { CountryCode.TF, "TF" },
            { CountryCode.TG, "TG" },
            { CountryCode.TH, "TH" },
            { CountryCode.TJ, "TJ" },
            { CountryCode.TK, "TK" },
            { CountryCode.TL, "TL" },
            { CountryCode.TM, "TM" },
            { CountryCode.TN, "TN" },
            { CountryCode.TO, "TO" },
            { CountryCode.TR, "TR" },
            { CountryCode.TT, "TT" },
            { CountryCode.TV, "TV" },
            { CountryCode.TW, "TW" },
            { CountryCode.TZ, "TZ" },
            { CountryCode.UA, "UA" },
            { CountryCode.UG, "UG" },
            { CountryCode.UM, "UM" },
            { CountryCode.US, "US" },
            { CountryCode.UY, "UY" },
            { CountryCode.UZ, "UZ" },
            { CountryCode.VA, "VA" },
            { CountryCode.VC, "VC" },
            { CountryCode.VE, "VE" },
            { CountryCode.VG, "VG" },
            { CountryCode.VI, "VI" },
            { CountryCode.VN, "VN" },
            { CountryCode.VU, "VU" },
            { CountryCode.WF, "WF" },
            { CountryCode.WS, "WS" },
            { CountryCode.YE, "YE" },
            { CountryCode.YT, "YT" },
            { CountryCode.ZA, "ZA" },
            { CountryCode.ZM, "ZM" },
            { CountryCode.ZW, "ZW" }
        };
        public static BiLookup<CurrencyType, string> CurrencyTypeStrings = new BiLookup<CurrencyType, string>
        {
            { CurrencyType.AUD, "AUD" },
            { CurrencyType.CAD, "CAD" },
            { CurrencyType.CHF, "CHF" },
            { CurrencyType.CZK, "CZK" },
            { CurrencyType.DKK, "DKK" },
            { CurrencyType.EUR, "EUR" },
            { CurrencyType.GBP, "GBP" },
            { CurrencyType.HKD, "HKD" },
            { CurrencyType.HUF, "HUF" },
            { CurrencyType.ILS, "ILS" },
            { CurrencyType.JPY, "JPY" },
            { CurrencyType.MXN, "MXN" },
            { CurrencyType.NOK, "NOK" },
            { CurrencyType.NONE, "" },
            { CurrencyType.NZD, "NZD" },
            { CurrencyType.PHP, "PHP" },
            { CurrencyType.PLN, "PLN" },
            { CurrencyType.SEK, "SEK" },
            { CurrencyType.SGD, "SGD" },
            { CurrencyType.THB, "THB" },
            { CurrencyType.TWD, "TWD" },
            { CurrencyType.USD, "USD" }
        };
        public static BiLookup<CustomFieldValue, string> CustomFieldValueStrings = new BiLookup<CustomFieldValue, string>
        {
            { CustomFieldValue.CustomField1, "CustomField1" },
            { CustomFieldValue.CustomField2, "CustomField2" },
            { CustomFieldValue.CustomField3, "CustomField3" },
            { CustomFieldValue.CustomField4, "CustomField4" },
            { CustomFieldValue.CustomField5, "CustomField5" },
            { CustomFieldValue.CustomField6, "CustomField6" },
            { CustomFieldValue.CustomField7, "CustomField7" },
            { CustomFieldValue.CustomField8, "CustomField8" },
            { CustomFieldValue.CustomField9, "CustomField9" },
            { CustomFieldValue.CustomField10, "CustomField10" },
            { CustomFieldValue.CustomField11, "CustomField11" },
            { CustomFieldValue.CustomField12, "CustomField12" },
            { CustomFieldValue.CustomField13, "CustomField13" },
            { CustomFieldValue.CustomField14, "CustomField14" },
            { CustomFieldValue.CustomField15, "CustomField15" }
        };
        public static BiLookup<EmailAddressConfirmStatus, string> EmailAddressConfirmStatusStrings = new BiLookup<EmailAddressConfirmStatus, string>
        {
            { EmailAddressConfirmStatus.Confirmed, "CONFIRMED" },
            { EmailAddressConfirmStatus.NoConfirmationRequired, "NO_CONFIRMATION_REQURIED" },
            { EmailAddressConfirmStatus.Unconfirmed, "UNCONFIRMED" },
        };
        public static BiLookup<EmailAddressStatus, string> EmailAddressStatusStrings = new BiLookup<EmailAddressStatus, string>
        {
            { EmailAddressStatus.None, "" },
            { EmailAddressStatus.All, "All" },
            { EmailAddressStatus.Confirmed, "CONFIRMED" },
            { EmailAddressStatus.Unconfirmed, "UNCONFIRMED" }
        };
        public static BiLookup<EmailCampaignStatus, string> EmailCampaignStatusStrings = new BiLookup<EmailCampaignStatus, string>
        {
            { EmailCampaignStatus.None, "" },
            { EmailCampaignStatus.All, "ALL" },
            { EmailCampaignStatus.Draft, "DRAFT" },
            { EmailCampaignStatus.Running, "RUNNING" },
            { EmailCampaignStatus.Sent, "SENT" },
            { EmailCampaignStatus.Scheduled, "SCHEDULED" }            
        };
        public static BiLookup<EmailContentFormat, string> EmailContentFormatStrings = new BiLookup<EmailContentFormat, string>
        {
            { EmailContentFormat.None, "" },
            { EmailContentFormat.Html, "HTML" },
            { EmailContentFormat.Xhtml, "XHTML" }
        };
        public static BiLookup<EmailFormat, string> EmailFormatStrings = new BiLookup<EmailFormat, string>
        { 
            { EmailFormat.Html, "HTML" }, 
            { EmailFormat.Text, "TEXT" }, 
            { EmailFormat.HtmlAndText, "HTML_AND_TEXT" }
        };
        public static BiLookup<EventSpotFeeScope, string> EventSpotFeeScopeStrings = new BiLookup<EventSpotFeeScope, string>
        {
            { EventSpotFeeScope.Both, "BOTH" }, 
            { EventSpotFeeScope.Guests, "GUESTS" }, 
            { EventSpotFeeScope.Registrants, "REGISTRANTS" }
        };
        public static BiLookup<EventSpotInformationSection, string> EventSpotInformationSectionStrings = new BiLookup<EventSpotInformationSection, string>
        {
            { EventSpotInformationSection.Contact, "CONTACT" },
            { EventSpotInformationSection.Location, "LOCATION" },
            { EventSpotInformationSection.Time, "TIME" },
        };
        public static BiLookup<EventSpotStatus, string> EventSpotStatusStrings = new BiLookup<EventSpotStatus, string>
        {
            { EventSpotStatus.Active, "ACTIVE" },
            { EventSpotStatus.Cancelled, "CANCELLED" },
            { EventSpotStatus.Complete, "COMPLETE" },
            { EventSpotStatus.Deleted, "DELETED" },
            { EventSpotStatus.Draft, "DRAFT" },
        };
        public static BiLookup<EventSpotType, string> EventSpotTypeStrings = new BiLookup<EventSpotType, string>
        {
            { EventSpotType.Auction, "AUCTION" }, 
            { EventSpotType.Birthday, "BIRTHDAY" }, 
            { EventSpotType.BusinessFinanceSales, "BUSINESS_FINANCE_SALES" }, 
            { EventSpotType.ClassesWorkshops, "CLASSES_WORKSHOPS" }, 
            { EventSpotType.CompetitionSports, "COMPETITION_SPORTS" }, 
            { EventSpotType.ConferencesSeminarsForum, "CONFERENCES_SEMINARS_FORUM" }, 
            { EventSpotType.ConventionsTradeshowsExpos, "CONVENTIONS_TRADESHOWS_EXPOS" }, 
            { EventSpotType.FestivalsFairs, "FESTIVALS_FAIRS" }, 
            { EventSpotType.FoodWine, "FOOD_WINE" }, 
            { EventSpotType.FundraisersCharities, "FUNDRAISERS_CHARITIES" }, 
            { EventSpotType.Holiday, "HOLIDAY" }, 
            { EventSpotType.IncentiveRewardRecognition, "INCENTIVE_REWARD_RECOGNITION" }, 
            { EventSpotType.MoviesFilm, "MOVIES_FILM" }, 
            { EventSpotType.MusicConcerts, "MUSIC_CONCERTS" }, 
            { EventSpotType.NetworkingClubs, "NETWORKING_CLUBS" }, 
            { EventSpotType.PerformingArts, "PERFORMING_ARTS" },
            { EventSpotType.Other, "OTHER" },
            { EventSpotType.OutdoorsRecreation, "OUTDOORS_RECREATION" },         
            { EventSpotType.ReligionSpirituality, "RELIGION_SPIRITUALITY" }, 
            { EventSpotType.SchoolsReunionsAlumni, "SCHOOLS_REUNIONS_ALUMNI" }, 
            { EventSpotType.PartiesSocialEventsMixers, "PARTIES_SOCIAL_EVENTS_MIXERS" }, 
            { EventSpotType.Travel, "TRAVEL" }, 
            { EventSpotType.WebinarTeleseminarTeleclass, "WEBINAR_TELESEMINAR_TELECLASS" },
            { EventSpotType.Weddings, "WEDDINGS" }        
        };
        public static BiLookup<ExportColumnName, string> ExportColumnNameStrings = new BiLookup<ExportColumnName, string>
        {
            { ExportColumnName.AddressLine1, "ADDRESS LINE 1" },
            { ExportColumnName.AddressLine2, "ADDRESS LINE 2" },
            { ExportColumnName.AddressLine3, "ADDRESS LINE 3" },
            { ExportColumnName.City, "CITY" },
            { ExportColumnName.CompanyName, "COMPANY NAME" },
            { ExportColumnName.Country, "COUNTRY" },
            { ExportColumnName.CustomField1, "CUSTOM FIELD 1" },
            { ExportColumnName.CustomField2, "CUSTOM FIELD 2" },
            { ExportColumnName.CustomField3, "CUSTOM FIELD 3" },
            { ExportColumnName.CustomField4, "CUSTOM FIELD 4"  },
            { ExportColumnName.CustomField5, "CUSTOM FIELD 5" },
            { ExportColumnName.CustomField6, "CUSTOM FIELD 6" },
            { ExportColumnName.CustomField7, "CUSTOM FIELD 7" },
            { ExportColumnName.CustomField8, "CUSTOM FIELD 8" },
            { ExportColumnName.CustomField9, "CUSTOM FIELD 9" },
            { ExportColumnName.CustomField10, "CUSTOM FIELD 10" },
            { ExportColumnName.CustomField11, "CUSTOM FIELD 11" },
            { ExportColumnName.CustomField12, "CUSTOM FIELD 12" },
            { ExportColumnName.CustomField13, "CUSTOM FIELD 13" },
            { ExportColumnName.CustomField14, "CUSTOM FIELD 14" },
            { ExportColumnName.CustomField15, "CUSTOM FIELD 15" },
            { ExportColumnName.Email, "EMAIL" },
            { ExportColumnName.FirstName, "FIRST NAME" },
            { ExportColumnName.HomePhone, "HOME PHONE" },
            { ExportColumnName.JobTitle, "JOB TITLE" },
            { ExportColumnName.LastName, "LAST NAME" },
            { ExportColumnName.State, "STATE" },
            { ExportColumnName.USStateCAProvince, "US State/CA Province" },
            { ExportColumnName.WorkPhone, "WORK PHONE" },
            { ExportColumnName.ZipPostalCode, "ZIP/POSTAL CODE" }
        };
        public static BiLookup<GreetingName, string> GreetingNameStrings = new BiLookup<GreetingName, string>
        {
            { GreetingName.None, "NONE" },
            { GreetingName.FirstName, "FIRST_NAME" },
            { GreetingName.LastName, "LAST_NAME" },
            { GreetingName.FirstAndLastName, "FIRST_AND_LAST_NAME" }
        };
        public static BiLookup<ImportColumnName, string> ImportColumnNameStrings = new BiLookup<ImportColumnName, string>
        {
            { ImportColumnName.AddressLine1, "ADDRESS LINE 1" },
            { ImportColumnName.AddressLine2, "ADDRESS LINE 2" },
            { ImportColumnName.AddressLine3, "ADDRESS LINE 3" },
            { ImportColumnName.Anniversary, "ANNIVERSARY" },
            { ImportColumnName.BirthdayDay, "BIRTHDAY_DAY" },
            { ImportColumnName.BirthdayMonth, "BIRTHDAY_MONTH" },
            { ImportColumnName.City, "CITY" },
            { ImportColumnName.CompanyName, "COMPANY NAME" },
            { ImportColumnName.Country, "COUNTRY" },
            { ImportColumnName.CustomField1, "CUSTOM FIELD 1" },
            { ImportColumnName.CustomField2, "CUSTOM FIELD 2" },
            { ImportColumnName.CustomField3, "CUSTOM FIELD 3" },
            { ImportColumnName.CustomField4, "CUSTOM FIELD 4" },
            { ImportColumnName.CustomField5, "CUSTOM FIELD 5" },
            { ImportColumnName.CustomField6, "CUSTOM FIELD 6" },
            { ImportColumnName.CustomField7, "CUSTOM FIELD 7" },
            { ImportColumnName.CustomField8, "CUSTOM FIELD 8" },
            { ImportColumnName.CustomField9, "CUSTOM FIELD 9" },
            { ImportColumnName.CustomField10, "CUSTOM FIELD 10" },
            { ImportColumnName.CustomField11, "CUSTOM FIELD 11" },
            { ImportColumnName.CustomField12, "CUSTOM FIELD 12" },
            { ImportColumnName.CustomField13, "CUSTOM FIELD 13" },
            { ImportColumnName.CustomField14, "CUSTOM FIELD 14" },
            { ImportColumnName.CustomField15, "CUSTOM FIELD 15" },
            { ImportColumnName.Email, "EMAIL" },
            { ImportColumnName.FirstName, "FIRST NAME" },
            { ImportColumnName.HomePhone, "HOME PHONE" },
            { ImportColumnName.JobTitle, "JOB TITLE" },
            { ImportColumnName.LastName, "LAST NAME" },
            { ImportColumnName.State, "STATE" },
            { ImportColumnName.WorkPhone, "WORK PHONE" },
            { ImportColumnName.ZipPostalCode, "ZIP/POSTAL CODE" },
        };
        public static BiLookup<LibraryFileType, string> LibraryFileTypeStrings = new BiLookup<LibraryFileType, string>
        {
            { LibraryFileType.Jpeg, "JPEG" }, 
            { LibraryFileType.Jpg, "JPG" }, 
            { LibraryFileType.Gif, "GIF" }, 
            { LibraryFileType.Pdf, "PDF" }, 
            { LibraryFileType.Png, "PNG" }, 
            { LibraryFileType.Doc, "DOC" }, 
            { LibraryFileType.Docx, "DOCX" }, 
            { LibraryFileType.Xls, "XLS" }, 
            { LibraryFileType.Xlsx, "XLSX" }, 
            { LibraryFileType.Ppt, "PPT" }, 
            { LibraryFileType.Pptx, "PPTX" }
        };
        public static BiLookup<LibrarySortBy, string> LibrarySortByStrings = new BiLookup<LibrarySortBy, string>
        { 
            { LibrarySortBy.CreatedDateDesc, "CREATED_DATE_DESC" }, 
            { LibrarySortBy.CreatedDate, "CREATED_DATE" }, 
            { LibrarySortBy.ModifiedDate, "MODIFIED_DATE" }, 
            { LibrarySortBy.ModifiedDateDesc, "MODIFIED_DATE_DESC" }, 
            { LibrarySortBy.Name, "NAME" }, 
            { LibrarySortBy.NameDesc, "NAME_DESC" }, 
            { LibrarySortBy.Size, "SIZE" }, 
            { LibrarySortBy.SizeDesc, "SIZE_DESC" }, 
            { LibrarySortBy.Dimension, "DIMENSION" }, 
            { LibrarySortBy.DimensionDesc, "DIMENSION_DESC" }
        };
        public static BiLookup<LibrarySource, string> LibrarySourceStrings = new BiLookup<LibrarySource, string>
        {
            { LibrarySource.All, "ALL" }, 
            { LibrarySource.MyComputer, "MyComputer" }, 
            { LibrarySource.Facebook, "Facebook" }, 
            { LibrarySource.Instagram, "Instagram" }, 
            { LibrarySource.Shutterstock, "Shutterstock" }, 
            { LibrarySource.Mobile, "Mobile" }
        };
        public static BiLookup<LibraryStatus, string> LibraryStatusStrings = new BiLookup<LibraryStatus, string>
        {
            { LibraryStatus.Active, "Active" }, 
            { LibraryStatus.Processing, "Processing" }, 
            { LibraryStatus.Uploaded, "Uploaded" }, 
            { LibraryStatus.VirusFound, "VirusFound" }, 
            { LibraryStatus.Failed, "Failed" }, 
            { LibraryStatus.Deleted, "Deleted" }
        };
        public static BiLookup<LibraryType, string> LibraryTypeStrings = new BiLookup<LibraryType, string>
        {
            { LibraryType.All, "ALL" }, 
            { LibraryType.Images, "IMAGES" }, 
            { LibraryType.Documents, "DOCUMENTS" }
        };
        public static BiLookup<OptInSource, string> OptInSourceStrings = new BiLookup<OptInSource, string>
        {
            { OptInSource.None, ""},
            { OptInSource.ActionByOwner, "ACTION_BY_OWNER" },
            { OptInSource.ActionBySystem, "ACTION_BY_SYSTEM" },
            { OptInSource.ActionByVisitor, "ACTION_BY_VISITOR" },
        };
        public static BiLookup<OptOutSource, string> OptOutSourceStrings = new BiLookup<OptOutSource, string>
        {
            { OptOutSource.None, "" },
            { OptOutSource.ActionByOwner, "ACTION_BY_OWNER" },            
            { OptOutSource.ActionByVisitor, "ACTION_BY_VISITOR" },
        };
        public static BiLookup<PaymentType, string> PaymentTypeStrings = new BiLookup<PaymentType, string>
        {
            { PaymentType.Check, "CHECK" },
            { PaymentType.Door, "DOOR" },
            { PaymentType.GoogleCheckout, "GOOGLE_CHECKOUT" },
            { PaymentType.PayPal, "PAYPAL" },
        };
        public static BiLookup<PromoCodeDiscountScope, string> PromoCodeDiscountScopeStrings = new BiLookup<PromoCodeDiscountScope, string>
        {
            { PromoCodeDiscountScope.None, "NONE" },
            { PromoCodeDiscountScope.FeeList, "FEE_LIST" },
            { PromoCodeDiscountScope.OrderTotal, "ORDER_TOTAL" },
        };
        public static BiLookup<PromoCodeType, string> PromoCodeTypeStrings = new BiLookup<PromoCodeType, string>
        {
            { PromoCodeType.Access, "ACCESS" },
            { PromoCodeType.Discount, "DISCOUNT" },
        };
        public static BiLookup<ServiceEndpoint, string> ServiceEndpointStrings = new BiLookup<ServiceEndpoint, string>
        {
            { ServiceEndpoint.Activities, "activities/" },
            { ServiceEndpoint.AccountInfo, "account/info" }, 
            { ServiceEndpoint.AddContacts, "activities/addcontacts" }, 
            { ServiceEndpoint.Bounces, "tracking/bounces" }, 
            { ServiceEndpoint.ClearLists, "activities/clearlists" }, 
            { ServiceEndpoint.Clicks, "tracking/clicks" }, 
            { ServiceEndpoint.ContactList, "lists" },
            { ServiceEndpoint.Contacts, "contacts" }, 
            { ServiceEndpoint.EmailCampaigns, "emailmarketing/campaigns" }, 
            { ServiceEndpoint.EventSpotEvents, "eventspot/events" }, 
            { ServiceEndpoint.ExportContacts, "activities/exportcontacts" }, 
            { ServiceEndpoint.ExportFiles, "activities/exportfiles" }, 
            { ServiceEndpoint.Forwards, "tracking/forwards" }, 
            { ServiceEndpoint.Library, "library/info" }, 
            { ServiceEndpoint.LibraryFiles, "library/files" }, 
            { ServiceEndpoint.LibraryFilesUploadStatus, "library/files/uploadstatus" },
            { ServiceEndpoint.LibraryFolders, "library/folders" }, 
            { ServiceEndpoint.LibraryTrashFolder, "library/folders/trash/files" }, 
            { ServiceEndpoint.Opens, "tracking/opens" }, 
            { ServiceEndpoint.RemoveFromLists, "activities/removefromlists" }, 
            { ServiceEndpoint.Sends, "tracking/sends" }, 
            { ServiceEndpoint.Summary, "tracking/reports/summary" }, 
            { ServiceEndpoint.SummaryByCampaign, "tracking/reports/summaryByCampaign" }, 
            { ServiceEndpoint.Tracking, "tracking" }, 
            { ServiceEndpoint.Unsubscribes, "tracking/unsubscribes" }, 
            { ServiceEndpoint.VerifiedEmailAddress, "account/verifiedemailaddresses" }
        };
        public static BiLookup<SdkBoolean, string> SdkBooleanStrings = new BiLookup<SdkBoolean, string>
        { 
            { SdkBoolean.True, "TRUE" }, 
            { SdkBoolean.False, "FALSE" }
        };
        public static BiLookup<TemplateType, string> TemplateTypeStrings = new BiLookup<TemplateType, string>
        {
            { TemplateType.None, "" },
            { TemplateType.Custom, "CUSTOM" },
            { TemplateType.Stock, "STOCK" },
            { TemplateType.TemplateV2, "TEMPLATE_V2" }
        };
        public static BiLookup<TimeZone, string> TimeZoneStrings = new BiLookup<TimeZone, string>
        {
            { TimeZone.Africa_Accra, "Africa/Accra" },
            { TimeZone.Africa_Casablanca, "Africa/Casablanca" },
            { TimeZone.Africa_Harare, "Africa/Harare" },
            { TimeZone.Africa_Johannesburg, "Africa/Johannesburg" },
            { TimeZone.Africa_Lagos, "Africa/Lagos" },
            { TimeZone.Africa_Monrovia, "Africa/Monrovia" },
            { TimeZone.Africa_Nairobi, "Africa/Nairobi" },
            { TimeZone.Africa_Windhoek, "Africa/Windhoek" },
            { TimeZone.America_Asuncion, "America/Asuncion" },
            { TimeZone.America_Bogota, "America/Bogota" },
            { TimeZone.America_Buenos_Aires, "America/Buenos_Aires" },
            { TimeZone.America_Caracas, "America/Caracas" },
            { TimeZone.America_Cayenne, "America/Cayenne" },
            { TimeZone.America_Chihuahua, "America/Chihuahua" },
            { TimeZone.America_Cuiaba, "America/Cuiaba" },
            { TimeZone.America_Fortaleza, "America/Fortaleza" },
            { TimeZone.America_Godthab, "America/Godthab" },
            { TimeZone.America_Guayaquil, "America/Guayaquil" },
            { TimeZone.America_Guyana, "America/Guyana" },
            { TimeZone.America_La_Paz, "America/La_Paz" },
            { TimeZone.America_Lima, "America/Lima" },
            { TimeZone.America_Manaus, "America/Manaus" },
            { TimeZone.America_Mexico_City, "America/Mexico_City" },
            { TimeZone.America_Montevideo, "America/Montevideo" },
            { TimeZone.America_Paramaribo, "America/Paramaribo" },
            { TimeZone.America_Puerto_Rico, "America/Puerto_Rico" },
            { TimeZone.America_Santiago, "America/Santiago" },
            { TimeZone.Asia_Almaty, "Asia/Almaty" },
            { TimeZone.Asia_Amman, "Asia/Amman" },
            { TimeZone.Asia_Aqtob, "Asia/Aqtob" },
            { TimeZone.Asia_Ashgabat, "Asia/Ashgabat" },
            { TimeZone.Asia_Baghdad, "Asia/Baghdad" },
            { TimeZone.Asia_Baku, "Asia/Baku" },
            { TimeZone.Asia_Bangkok, "Asia/Bangkok" },
            { TimeZone.Asia_Beirut, "Asia/Beirut" },
            { TimeZone.Asia_Bishkek, "Asia/Bishkek" },
            { TimeZone.Asia_Brunei, "Asia/Brunei" },
            { TimeZone.Asia_Chongqing, "Asia/Chongqing" },
            { TimeZone.Asia_Dhaka, "Asia/Dhaka" },
            { TimeZone.Asia_Dili, "Asia/Dili" },
            { TimeZone.Asia_Dubai, "Asia/Dubai" },
            { TimeZone.Asia_Dushanbe, "Asia/Dushanbe" },
            { TimeZone.Asia_Hong_Kong, "Asia/Hong_Kong" },
            { TimeZone.Asia_Irkutsk, "Asia/Irkutsk" },
            { TimeZone.Asia_Jakarta, "Asia/Jakarta" },
            { TimeZone.Asia_Jayapura, "Asia/Jayapura" },
            { TimeZone.Asia_Jerusalem, "Asia/Jerusalem" },
            { TimeZone.Asia_Kabul, "Asia/Kabul" },
            { TimeZone.Asia_Kamchatka, "Asia/Kamchatka" },
            { TimeZone.Asia_Karachi, "Asia/Karachi" },
            { TimeZone.Asia_Kathmandu, "Asia/Kathmandu" },
            { TimeZone.Asia_Kolkata, "Asia/Kolkata" },
            { TimeZone.Asia_Krasnoyarsk, "Asia/Krasnoyarsk" },
            { TimeZone.Asia_Kuala_Lumpur, "Asia/Kuala_Lumpur" },
            { TimeZone.Asia_Kuwait, "Asia/Kuwait" },
            { TimeZone.Asia_Magadan, "Asia/Magadan" },
            { TimeZone.Asia_Makassar, "Asia/Makassar" },
            { TimeZone.Asia_Manila, "Asia/Manila" },
            { TimeZone.Asia_Novosibirsk, "Asia/Novosibirsk" },
            { TimeZone.Asia_Omsk, "Asia/Omsk" },
            { TimeZone.Asia_Oral, "Asia/Oral" },
            { TimeZone.Asia_Qyzylorda, "Asia/Qyzylorda" },
            { TimeZone.Asia_Rangoon, "Asia/Rangoon" },
            { TimeZone.Asia_Seoul, "Asia/Seoul" },
            { TimeZone.Asia_Singapore, "Asia/Singapore" },
            { TimeZone.Asia_Taipei, "Asia/Taipei" },
            { TimeZone.Asia_Tashkent, "Asia/Tashkent" },
            { TimeZone.Asia_Tbilisi, "Asia/Tbilisi" },
            { TimeZone.Asia_Tehran, "Asia/Tehran" },
            { TimeZone.Asia_Thimbu, "Asia/Thimbu" },
            { TimeZone.Asia_Tokyo, "Asia/Tokyo" },
            { TimeZone.Asia_Ulaanbaatar, "Asia/Ulaanbaatar" },
            { TimeZone.Asia_Vladivostok, "Asia/Vladivostok" },
            { TimeZone.Asia_Yakutsk, "Asia/Yakutsk" },
            { TimeZone.Asia_Yekaterinburg, "Asia/Yekaterinburg" },
            { TimeZone.Asia_Yerevan, "Asia/Yerevan" },
            { TimeZone.Atlantic_Azores, "Atlantic/Azores" },
            { TimeZone.Atlantic_Cape_Verde, "Atlantic/Cape_Verde" },
            { TimeZone.Atlantic_South_Georgia, "Atlantic/South_Georgia" },
            { TimeZone.Australia_Adelaide, "Australia/Adelaide" },
            { TimeZone.Australia_Brisbane, "Australia/Brisbane" },
            { TimeZone.Australia_Canberra, "Australia/Canberra" },
            { TimeZone.Australia_Darwin, "Australia/Darwin" },
            { TimeZone.Australia_Hobart, "Australia/Hobart" },
            { TimeZone.Australia_Perth, "Australia/Perth" },
            { TimeZone.Brazil_East, "Brazil/East" },
            { TimeZone.CST, "CST" },
            { TimeZone.Canada_Atlantic, "Canada/Atlantic" },
            { TimeZone.Canada_Newfoundland, "Canada/Newfoundland" },
            { TimeZone.Canada_Saskatchewan, "Canada/Saskatchewan" },
            { TimeZone.Etc_Gtm_Plus_12, "Etc/GMT+12" },
            { TimeZone.Etc_Universal, "Etc/Universal" },
            { TimeZone.Europe_Amsterdam, "Europe/Amsterdam" },
            { TimeZone.Europe_Athens, "Europe/Athens" },
            { TimeZone.Europe_Belgrade, "Europe/Belgrade" },
            { TimeZone.Europe_Brussels, "Europe/Brussels" },
            { TimeZone.Europe_Helsinki, "Europe/Helsinki" },
            { TimeZone.Europe_Lisbon, "Europe/Lisbon" },
            { TimeZone.Europe_London, "Europe/London" },
            { TimeZone.Europe_Minsk, "Europe/Minsk" },
            { TimeZone.Europe_Moscow, "Europe/Moscow" },
            { TimeZone.Europe_Sarajevo, "Europe/Sarajevo" },
            { TimeZone.Europe_Volgograd, "Europe/Volgograd" },
            { TimeZone.Indian_Mahe, "Indian/Mahe" },
            { TimeZone.Indian_Maldives, "Indian/Maldives" },
            { TimeZone.Indian_Mauritius, "Indian/Mauritius" },
            { TimeZone.Indian_Reunion, "Indian/Reunion" },
            { TimeZone.Mexico_BajaNorte, "Mexico/BajaNorte" },
            { TimeZone.Pacific_Auckland, "Pacific/Auckland" },
            { TimeZone.Pacific_Efate, "Pacific/Efate" },
            { TimeZone.Pacific_Fiji, "Pacific/Fiji" },
            { TimeZone.Pacific_Guadalcanal, "Pacific/Guadalcanal" },
            { TimeZone.Pacific_Guam, "Pacific/Guam" },
            { TimeZone.Pacific_Noumea, "Pacific/Noumea" },
            { TimeZone.Pacific_Port_Moresby, "Pacific/Port_Moresby" },
            { TimeZone.Pacific_Samoa, "Pacific/Samoa" },
            { TimeZone.Pacific_Tahiti, "Pacific/Tahiti" },
            { TimeZone.Pacific_Tongatapu, "Pacific/Tongatapu" },
            { TimeZone.US_Alaska, "US/Alaska" },
            { TimeZone.US_Arizona, "US/Arizona" },
            { TimeZone.US_Central, "US/Central" },
            { TimeZone.US_East_Indiana, "US/East-Indiana" },
            { TimeZone.US_Eastern, "US/Eastern" },
            { TimeZone.US_Hawaii, "US/Hawaii" },
            { TimeZone.US_Mountain, "US/Mountain" },
            { TimeZone.US_Pacific, "US/Pacific" }
        };
    }
}