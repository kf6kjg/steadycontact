﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.Helpers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SteadyContactSdk.MyLibrary
{
    public interface IMyLibraryService
    {
        Task<PaginatedResult<LibraryFile>> GetLibraryFilesAsync(int limit = 50, LibrarySortBy sortBy = LibrarySortBy.CreatedDateDesc, LibrarySource source = LibrarySource.All, LibraryType type = LibraryType.All);
        Task<PaginatedResult<LibraryFile>> GetLibraryFilesByFolderAsync(string folderId);
        Task<LibraryFile> GetLibraryFileAsync(string fileId);
        Task<LibraryFile> UpdateLibraryFileAsync(string fileId, string folderId, string name, string description, SdkBoolean includePayload);
        Task DeleteLibraryFilesAsync(IEnumerable<string> fileIds);
        Task<string> AddLibraryFileAsync(string fileName, string folderId, string description, LibrarySource source, LibraryFileType fileType, byte[] data);
        Task<IEnumerable<LibraryFileStatus>> GetLibraryFilesStatusAsync(IEnumerable<string> fileIds);
        Task<IEnumerable<LibraryFileMoveResult>> MoveLibraryFilesAsync(IEnumerable<string> fileIds, string destinationFolderId);
        Task<LibrarySummaryInformation> GetLibrarySummaryInformationAsync();
        Task<PaginatedResult<LibraryFolder>> GetLibraryFolderCollectionAsync(int limit = 50, LibrarySortBy sortBy = LibrarySortBy.CreatedDateDesc);
        Task<LibraryFolder> CreateLibraryFolderAsync(string name, string parentFolderId);
        Task<LibraryFolder> GetLibraryFolderAsync(string folderId);
        Task<LibraryFolder> UpdateLibraryFolderAsync(string folderId, string name, string parentId, SdkBoolean includePayload);
        Task DeleteLibraryFolderAsync(string folderId);
        Task<PaginatedResult<LibraryFile>> GetTrashFilesAsync(int limit = 50, LibrarySortBy sortBy = LibrarySortBy.CreatedDateDesc, LibraryType type = LibraryType.All);
        Task DeleteTrashFilesAsync();
    }
}