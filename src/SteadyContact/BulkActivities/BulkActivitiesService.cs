﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.BulkActivities.Models;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Models;
using SteadyContactSdk.Reports;
using SteadyContactSdk.Validation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SteadyContactSdk.BulkActivities
{
    public class BulkActivitiesService : ServiceBase, IBulkActivitiesService
    {
        public BulkActivitiesService(ISteadyContactSettings settings)
            : base(settings)
        { }

        internal BulkActivitiesService(ISteadyContactSettings settings, IValidationService validationService, IWebServiceRequest webServiceRequest)
            : base(settings, validationService, webServiceRequest)
        { }


        // Reference http://developer.constantcontact.com/docs/bulk_activities_api/bulks-activities-import-contacts-api.html
        public async Task<BulkActivityResponse> BulkUploadContactFileAsync(byte[] data, string fileName, BulkUploadFileFormat fileFormat, string lists)
        {
            return await BulkContactActivityAsync(data, fileName, fileFormat, lists, true);
        }

        // Reference http://developer.constantcontact.com/docs/bulk_activities_api/bulk-activities-removecontacts-multipart.html
        public async Task<BulkActivityResponse> BulkDeleteContactFileAsync(byte[] data, string fileName, BulkUploadFileFormat fileFormat, string lists)
        {
            return await BulkContactActivityAsync(data, fileName, fileFormat, lists, false);
        }

        private async Task<BulkActivityResponse> BulkContactActivityAsync(byte[] data, string fileName, BulkUploadFileFormat fileFormat, string lists, bool createContacts)
        {
            var mimeType = EnumStrings.BulkUploadFileFormatStrings[fileFormat];

            var model = new BulkContactActivityModel { FileName = fileName, Data = data, Lists = lists, MimeType = mimeType };
            validationService.Validate(model);

            var postParameters = new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("file_name", fileName),
                new KeyValuePair<string, object>("lists", lists),
                new KeyValuePair<string, object>("data", data)
            };

            var bulkEndpoint = createContacts ? ServiceEndpoint.AddContacts : ServiceEndpoint.RemoveFromLists;

            string endpoint = new SimpleModel(bulkEndpoint).GenerateEndpoint(settings);
            return await webServiceRequest.PostMultiparttDeserializedAsync<BulkActivityResponse>(new Uri(endpoint), settings.AuthToken, postParameters);
        }

        // Reference http://developer.constantcontact.com/docs/bulk_activities_api/bulk-activities-clear-contactlists.html
        public async Task<BulkActivityResponse> ClearContactListsAsync(ContactLists lists)
        {
            validationService.Validate(lists);

            string endpoint = new SimpleModel(ServiceEndpoint.ClearLists).GenerateEndpoint(settings);
            return await webServiceRequest.PostDeserializedAsync<ContactLists, BulkActivityResponse>(new Uri(endpoint), settings.AuthToken, lists);
        }

        // Reference http://developer.constantcontact.com/docs/bulk_activities_api/bulk-activities-export-contacts.html
        public async Task<BulkActivityResponse> ExportContactsAsync(ContactExport contactExport)
        {
            validationService.Validate(contactExport);

            string endpoint = new SimpleModel(ServiceEndpoint.ExportContacts).GenerateEndpoint(settings);
            return await webServiceRequest.PostDeserializedAsync<ContactExport, BulkActivityResponse>(new Uri(endpoint), settings.AuthToken, contactExport);
        }

        // Reference http://developer.constantcontact.com/docs/bulk_activities_api/bulk-activities-status-detail-report-api.html
        public async Task<ActivityStatus> GetActivityStatusAsync(string activityId)
        {
            var model = new ActivityIdModel { Settings = settings, ActivityId = activityId };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<ActivityStatus>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference http://developer.constantcontact.com/docs/bulk_activities_api/bulk-activities-summary-report-api.html
        public async Task<IEnumerable<ActivityStatusReport>> GetBulkActivityStatusReportAsync(BulkActivityType type = BulkActivityType.None, BulkActivityStatus status = BulkActivityStatus.All)
        {
            var model = new BulkActivityStatusReportModel { Settings = settings, Type = type, Status = status };
            return await webServiceRequest.GetDeserializedAsync<IEnumerable<ActivityStatusReport>>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference http://developer.constantcontact.com/docs/bulk_activities_api/bulks-activities-import-contacts-api.html
        public async Task<BulkActivityResponse> ImportContactsAsync(ContactImport contactImport)
        {
            validationService.Validate(contactImport);

            string endpoint = new SimpleModel(ServiceEndpoint.AddContacts).GenerateEndpoint(settings);
            return await webServiceRequest.PostDeserializedAsync<ContactImport, BulkActivityResponse>(new Uri(endpoint), settings.AuthToken, contactImport);
        }

        // Reference http://developer.constantcontact.com/docs/bulk_activities_api/bulks-activities-remove-contacts.html
        public async Task<BulkActivityResponse> RemoveContactsAsync(ContactRemove contactRemove)
        {
            validationService.Validate(contactRemove);

            string endpoint = new SimpleModel(ServiceEndpoint.RemoveFromLists).GenerateEndpoint(settings);
            return await webServiceRequest.PostDeserializedAsync<ContactRemove, BulkActivityResponse>(new Uri(endpoint), settings.AuthToken, contactRemove);
        }

        // Reference http://developer.constantcontact.com/docs/bulk_activities_api/bulk-activities-export-contacts.html
        public async Task<string> RetrieveExportedFileAsync(string activityId, BulkExportFileFormat fileExtension = BulkExportFileFormat.Csv)
        {
            var model = new FileExportModel { Settings = settings, ActivityId = activityId, FileExtension = fileExtension };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<string>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }
    }
}