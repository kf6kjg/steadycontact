﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Collections.Generic;
using System.Linq;
using SteadyContactSdk.Account.Validation;
using SteadyContactSdk.Exceptions;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Validation;
using Newtonsoft.Json;

namespace SteadyContactSdk.Account
{
    public class AccountSummary : IValidatable
    {
        [JsonProperty("company_logo")]
        public string CompanyLogo { get; internal set; }
        [JsonProperty("country_code"), JsonConverter(typeof(TypeEnumConverter<CountryCode, BiLookup<CountryCode, string>>))]
        public CountryCode CountryCode { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("organization_addresses")]
        public IEnumerable<OrganizationAddress> OrganizationAddresses { get; set; }
        [JsonProperty("organization_name")]
        public string OrganizationName { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [JsonProperty("state_code")]
        public string StateCode { get; set; }
        [JsonProperty("time_zone")]
        public string TimeZone { get; set; }
        [JsonProperty("website")]
        public string Website { get; set; }

        public IEnumerable<ValidationError> Validate()
        {
            var errors = new List<ValidationError>();
            errors.AddRange(Validator.Validate<AccountSummary, AccountSummaryValidator>(this));

            if (this.OrganizationAddresses != null)
                errors.AddRange(this.OrganizationAddresses.First().Validate());

            return errors;
        }
    }
}