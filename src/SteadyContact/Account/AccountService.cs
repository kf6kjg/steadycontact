﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.Account.Models;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Models;
using SteadyContactSdk.Validation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SteadyContactSdk.Account
{
    public class AccountService : ServiceBase, IAccountService
    {
        public AccountService(ISteadyContactSettings settings)
            : base(settings)
        { }

        internal AccountService(ISteadyContactSettings settings, IValidationService validationService, IWebServiceRequest webRequestService)
            : base(settings, validationService, webRequestService)
        { }

        // Reference: http://developer.constantcontact.com/docs/account/verify-email-Addresses.html?method=POST
        public async Task<IEnumerable<AccountEmailAddress>> CreateEmailAddressAsync(string emailAddress)
        {
            var email = new EmailAddressModel { EmailAddress = emailAddress };
            validationService.Validate(email);

            var model = new List<EmailAddressModel>() { email };

            var endpoint = new SimpleModel(ServiceEndpoint.VerifiedEmailAddress).GenerateEndpoint(settings);
            return await webServiceRequest.PostDeserializedAsync<List<EmailAddressModel>, IEnumerable<AccountEmailAddress>>(new Uri(endpoint), settings.AuthToken, model);
        }

        // Reference: http://developer.constantcontact.com/docs/account/summary-information-api.html?method=GET
        public async Task<AccountSummary> GetAccountInfoAsync()
        {
            var endpoint = new SimpleModel(ServiceEndpoint.AccountInfo).GenerateEndpoint(settings);
            return await webServiceRequest.GetDeserializedAsync<AccountSummary>(new Uri(endpoint), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/account/verify-email-Addresses.html?method=GET
        public async Task<IEnumerable<AccountEmailAddress>> GetEmailAddressByStatusAsync(EmailAddressStatus status = EmailAddressStatus.All)
        {
            var model = new EmailAddressStatusModel { Settings = settings, Status = status };
            return await webServiceRequest.GetDeserializedAsync<IEnumerable<AccountEmailAddress>>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/account/summary-information-api.html?method=PUT
        public async Task<AccountSummary> UpdateAccountInfoAsync(AccountSummary accountSummary)
        {
            validationService.Validate(accountSummary);

            var endpoint = new SimpleModel(ServiceEndpoint.AccountInfo).GenerateEndpoint(settings);
            return await webServiceRequest.PutDeserializedAsync<AccountSummary, AccountSummary>(new Uri(endpoint), settings.AuthToken, accountSummary);
        }
    }
}