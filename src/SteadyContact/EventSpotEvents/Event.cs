﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System;
using System.Collections.Generic;
using SteadyContactSdk.Exceptions;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Validation;
using Newtonsoft.Json;

namespace SteadyContactSdk.EventSpotEvents
{
    public class Event : IValidatable
    {
        [JsonProperty("active_date")]
        public string ActiveDate { get; internal set; }
        [JsonProperty("address")]
        public EventSpotAddress Address { get; set; }
        [JsonProperty("are_registrants_public")]
        public bool AreRegistrantsPublic { get; set; }
        [JsonProperty("cancelled_date")]
        public string CancelDate { get; internal set; }
        [JsonProperty("contact")]
        public EventSpotContact Contact { get; set; }
        [JsonProperty("created_date")]
        public string CreatedDate { get; internal set; }
        [JsonProperty("currency_type"), JsonConverter(typeof(TypeEnumConverter<CurrencyType, BiLookup<CurrencyType, string>>))]
        public CurrencyType CurrencyType { get; set; }
        [JsonProperty("deleted_date")]
        public string DeletedDate { get; internal set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("end_date")]
        public DateTime EndDate { get; set; }
        [JsonProperty("google_analytics_key")]
        public string GoogleAnalyticsKey { get; set; }
        [JsonProperty("google_merchant_id")]
        public string GoogleMerchantId { get; set; }
        [JsonProperty("id")]
        public string Id { get; internal set; }
        [JsonProperty("is_calendar_displayed")]
        public bool IsCalendarDisplayed { get; set; }
        [JsonProperty("is_checkin_available")]
        public bool IsCheckinAvailable { get; set; }
        [JsonProperty("is_home_page_displayed")]
        public bool IsHomePageDisplayed { get; set; }
        [JsonProperty("is_listed_in_external_directory")]
        public bool IsListedInExternalDirectory { get; set; }
        [JsonProperty("is_map_displayed")]
        public bool IsMapDisplayed { get; set; }
        [JsonProperty("is_virtual_event")]
        public bool IsVirtualEvent { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }
        [JsonProperty("meta_data_tags")]
        public string MetaDataTags { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("notification_options")]
        public IEnumerable<NotificationOptions> NotificationOptions { get; set; }
        [JsonProperty("online_meeting")]
        public IEnumerable<OnlineMeeting> OnlineMeeting { get; set; }
        [JsonProperty("payable_to")]
        public string PayableTo { get; set; }
        [JsonProperty("payment_address")]
        public EventSpotAddress PaymentAddress { get; set; }
        [JsonProperty("payment_options"), JsonConverter(typeof(TypeEnumConverter<PaymentType, BiLookup<PaymentType, string>>))]
        public IEnumerable<PaymentType> PaymentOptions { get; set; }
        [JsonProperty("paypal_account_email")]
        public string PayPalAccountEmail { get; set; }
        [JsonProperty("registration_url")]
        public string RegistrationUrl { get; internal set; }
        [JsonProperty("start_date")]
        public DateTime StartDate { get; set; }
        [JsonProperty("status"), JsonConverter(typeof(TypeEnumConverter<EventSpotStatus, BiLookup<EventSpotStatus, string>>))]
        public EventSpotStatus Status { get; internal set; }
        [JsonProperty("theme_name")]
        public string ThemeName { get; set; }
        [JsonProperty("time_zone_description")]
        public string TimeZoneDescription { get; set; }
        [JsonProperty("time_zone_id"), JsonConverter(typeof(TypeEnumConverter<SteadyContactSdk.Helpers.TimeZone, BiLookup<SteadyContactSdk.Helpers.TimeZone, string>>))]
        public SteadyContactSdk.Helpers.TimeZone TimeZoneId { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("total_registered_count")]
        public int TotalRegisteredCount { get; internal set; }
        [JsonProperty("track_information")]
        public TrackInformation TrackInformation { get; set; }
        [JsonProperty("twitter_hash_tag")]
        public string TwitterHashTag { get; set; }
        [JsonProperty("type"), JsonConverter(typeof(TypeEnumConverter<EventSpotType, BiLookup<EventSpotType, string>>))]
        public EventSpotType Type { get; set; }
        [JsonProperty("updated_date")]
        public string UpdatedDate { get; internal set; }

        public Event()
        {
            this.AreRegistrantsPublic = false;
            this.CurrencyType = CurrencyType.USD;
            this.IsCheckinAvailable = false;
            this.IsHomePageDisplayed = false;
            this.IsListedInExternalDirectory = false;
            this.IsMapDisplayed = false;
            this.IsVirtualEvent = false;
            this.ThemeName = "Default";
        }

        public IEnumerable<ValidationError> Validate()
        {
            var errors = new List<ValidationError>();
            errors.AddRange(Validator.Validate<Event, EventValidator>(this));

            if (this.Address != null)
                errors.AddRange(this.Address.Validate());

            if (this.Contact != null)
                errors.AddRange(this.Contact.Validate());

            if (this.OnlineMeeting != null)
                foreach (var mo in this.OnlineMeeting)
                    errors.AddRange(mo.Validate());

            if (this.PaymentAddress != null)
                errors.AddRange(this.PaymentAddress.Validate());

            if (this.TrackInformation != null)
                errors.AddRange(this.TrackInformation.Validate());

            return errors;
        }
    }
}