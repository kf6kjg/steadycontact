﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Collections.Generic;
using SteadyContactSdk.EventSpotEvents.Validation;
using SteadyContactSdk.Exceptions;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Validation;
using Newtonsoft.Json;

namespace SteadyContactSdk.EventSpotEvents
{
    [JsonObject("track_information")]
    public class TrackInformation : IValidatable
    {
        [JsonProperty("early_fee_date")]
        public string EarlyFeeDate { get; set; }
        [JsonProperty("guest_display_label")]
        public string GuestDisplayLabel { get; set; }
        [JsonProperty("guest_limit")]
        public int GuestLimit { get; set; }
        [JsonProperty("information_sections"), JsonConverter(typeof(TypeEnumConverter<EventSpotInformationSection, BiLookup<EventSpotInformationSection, string>>))]
        public IEnumerable<EventSpotInformationSection> InformationSections { get; set; }
        [JsonProperty("is_guest_anonymous_enabled")]
        public bool IsGuestAnonymousEnabled { get; set; }
        [JsonProperty("is_guest_name_required")]
        public bool IsGuestNameRequired { get; set; }
        [JsonProperty("is_registration_closed_manually")]
        public bool IsRegistrationClosedManually { get; set; }
        [JsonProperty("is_ticketing_link_displayed")]
        public bool IsTicketingLinkDisplayed { get; set; }
        [JsonProperty("late_fee_date")]
        public string LateFeeDate { get; set; }
        [JsonProperty("registration_limit_count")]
        public int RegistrationLimitCount { get; set; }
        [JsonProperty("registration_limit_date")]
        public string RegistrationLimitDate { get; set; }

        public TrackInformation()
        {
            this.GuestDisplayLabel = "Guest(s)";
            this.GuestLimit = 0;
            this.IsGuestAnonymousEnabled = false;
            this.IsGuestNameRequired = false;
            this.IsRegistrationClosedManually = false;
            this.IsTicketingLinkDisplayed = false;
        }

        public IEnumerable<ValidationError> Validate()
        {
            return Validator.Validate<TrackInformation, TrackInformationValidator>(this);
        }
    }
}