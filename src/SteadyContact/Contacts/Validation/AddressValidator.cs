﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;

namespace SteadyContactSdk.Contacts.Validation
{
    internal class AddressValidator : AbstractValidator<Address>
    {
        public AddressValidator()
        {           
            RuleFor(a => a.City).Length(0, 50).WithMessage("City cannot contain more than 50 characters");         
            RuleFor(a => a.Line1).Length(0, 50).WithMessage("Line1 cannot contain more than 50 characters");
            RuleFor(a => a.Line2).Length(0, 50).WithMessage("Line2 cannot contain more than 50 characters");
            RuleFor(a => a.Line3).Length(0, 50).WithMessage("Line3 cannot contain more than 50 characters");
            RuleFor(a => a.PostalCode).Length(0, 50).WithMessage("PostalCode cannot contain more than 50 characters");
            RuleFor(a => a.State).Length(0, 50).WithMessage("State cannot contain more than 50 characters");
            RuleFor(a => a.StateCode).Length(2).When(s => s.StateCode != null).WithMessage("StateCode must be exactly 2 characters");
            RuleFor(a => a.SubPostalCode).Length(0, 25).WithMessage("SubPostalCode cannot contain more than 50 characters");
            RuleFor(a => a.PostalCode).Must(pc => string.IsNullOrWhiteSpace(pc) || !pc.Contains(" ")).WithMessage("PostalCode contains a space, use SubPostalCode");
        }
    }
}