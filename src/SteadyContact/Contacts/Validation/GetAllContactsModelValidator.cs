﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;
using SteadyContactSdk.Contacts.Models;

namespace SteadyContactSdk.Contacts.Validation
{
    internal class GetAllContactsModelValidator : AbstractValidator<GetAllContactsModel>
    {
        public GetAllContactsModelValidator()
        {
            RuleFor(m => m.Email).EmailAddress().When(e => !string.IsNullOrWhiteSpace(e.Email)).WithMessage("Email is invalid");            
            RuleFor(m => m.Limit).GreaterThan(0).WithMessage("Limit must be between 1 and 500").LessThan(501).WithMessage("Limit must be between 1 and 500");
            RuleFor(m => m.ModifiedSince).ISO8601Date().WithMessage("ModifiedSince must be a in a ISO 8601 format and must be a date greater than 1/1/1970");
        }
    }
}