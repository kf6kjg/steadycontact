﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Collections.Generic;
using SteadyContactSdk.ContactLists.Validation;
using SteadyContactSdk.Exceptions;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Validation;
using Newtonsoft.Json;

namespace SteadyContactSdk.ContactLists
{
    public class ContactList : IValidatable
    {
        [JsonProperty("id")]
        public string Id { get; internal set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("status"), JsonConverter(typeof(TypeEnumConverter<ContactListsStatus, BiLookup<ContactListsStatus, string>>))]
        public ContactListsStatus Status { get; set; }
        [JsonProperty("Created_date")]
        public string CreatedDate { get; internal set; }
        [JsonProperty("modified_date")]
        public string ModifiedDate { get; internal set; }
        [JsonProperty("contact_count")]
        public int ContactCount { get; internal set; }

        public IEnumerable<ValidationError> Validate()
        {
            return Validator.Validate<ContactList, ContactListValidator>(this);
        }
    }
}