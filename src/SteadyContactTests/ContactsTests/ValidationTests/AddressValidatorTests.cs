﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.Contacts;
using SteadyContactSdk.Contacts.Validation;
using SteadyContactSdk.Helpers;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace SteadyContactTests.ContactsTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class AddressValidatorTests
    {
        private readonly AddressValidator validator;

        public AddressValidatorTests()
        {
            validator = new AddressValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void AddressType_Is_Null_Is_Valid()
        {
            // Arrange
            var address = new Address();

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        [Fact]
        public void AddressType_Is_Empty_Is_Valid()
        {
            // Arrange
            var address = new Address()
            {
                AddressType = ContactAddressType.None
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        [Fact]
        public void AddressType_Is_PERSONAL_Is_Valid()
        {
            // Arrange
            var address = new Address()
            {
                AddressType = ContactAddressType.Personal
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        [Fact]
        public void AddressType_Is_BUSINESS_Is_Valid()
        {
            // Arrange
            var address = new Address()
            {
                AddressType = ContactAddressType.Business
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        // *************************************************************************************************

        [Fact]
        public void City_Is_Longer_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var address = new Address()
            {
                City = "Pasadenawwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
            };

            // Act
            var result = validator.Validate(address);

            // Assert            
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "City cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Line1_Is_Longer_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var address = new Address()
            {
                Line1 = "California Institute of Technologywwwwwwwwwwwwwwwwwwww"
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Line1 cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Line2_Is_Longer_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var address = new Address()
            {
                Line2 = "California Institute of Technologywwwwwwwwwwwwwwwwwwww"
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Line2 cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Line3_Is_Longer_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var address = new Address()
            {
                Line3 = "California Institute of Technologywwwwwwwwwwwwwwwwwwww"
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Line3 cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Postal_Code_Is_Longer_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var address = new Address()
            {
                PostalCode = "91125wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PostalCode cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void State_Is_Longer_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var address = new Address()
            {
                State = "Californiawwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "State cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void State_Code_Is_1_Character_Generates_Error()
        {
            // Arrange
            var address = new Address()
            {
                StateCode = "C"
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "StateCode must be exactly 2 characters"));
        }

        [Fact]
        public void State_Code_Is_3_Characters_Generates_Error()
        {
            // Arrange
            var address = new Address()
            {
                StateCode = "CAL"
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "StateCode must be exactly 2 characters"));
        }

        [Fact]
        public void State_Code_Is_2_Characters_Is_Valid()
        {
            // Arrange
            var address = new Address()
            {
                StateCode = "CA"
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        // *************************************************************************************************

        [Fact]
        public void Sub_Postal_Code_Is_Longer_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var address = new Address()
            {
                SubPostalCode = "91125 1111wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "SubPostalCode cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Postal_Code_Has_Space_Should_Use_Sub_Postal_Code_Error_Is_Genderated()
        {
            // Arrange
            var address = new Address()
            {
                PostalCode = "91125 1111"
            };

            // Act
            var result = validator.Validate(address);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PostalCode contains a space, use SubPostalCode"));
        }
    }
}