﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.EmailCampaignTracking;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Models;
using SteadyContactSdk.Reports;
using SteadyContactSdk.Validation;
using Moq;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Xunit;

namespace SteadyContactTests.EmailCampaignTrackingTests
{
    [ExcludeFromCodeCoverage]
    public class EmailCampaignTrackingServiceTests
    {
        private EmailCampaignTrackingService emailCampaignTrackingService = null;
        private ISteadyContactSettings settings = null;
        private Mock<IValidationService> validationService = null;
        private Mock<IWebServiceRequest> webServiceRequest = null;

        public EmailCampaignTrackingServiceTests()
        {
            settings = new SteadyContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            validationService = new Mock<IValidationService>();
            validationService.Setup(m => m.Validate(It.IsAny<IValidatable>()));
            webServiceRequest = new Mock<IWebServiceRequest>();
        }

        public void InitializeEmailCampaignTrackingService<T>()
            where T : class, new()
        {
            emailCampaignTrackingService = new EmailCampaignTrackingService(settings, validationService.Object, webServiceRequest.Object);
        }

        // *************************************************************************************************       

        [Fact]
        public async Task GetActivitySummaryReportAsync_UpdateSummary_True_Uses_Correct_Endpoint()
        {            
            // Arrange
            InitializeEmailCampaignTrackingService<ActivitySummaryReport>();
            string campaignId = "123456";
            var updateSummary = true;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<ActivitySummaryReport>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new ActivitySummaryReport());

            // Act
            await emailCampaignTrackingService.GetActivitySummaryReportAsync(campaignId, updateSummary);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/tracking/reports/summary?api_key={2}&updateSummary=true", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);            
        }

        [Fact]
        public async Task GetActivitySummaryReportAsync_UpdateSummary_False_Uses_Correct_Endpoint()
        {            
            // Arrange
            InitializeEmailCampaignTrackingService<ActivitySummaryReport>();
            string campaignId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<ActivitySummaryReport>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new ActivitySummaryReport());

            // Act
            await emailCampaignTrackingService.GetActivitySummaryReportAsync(campaignId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/tracking/reports/summary?api_key={2}", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);            
        }

        [Fact]
        public async Task GetActivitySummaryReportAsync_Verify_MakeSerializedRequestAsync()
        {            
            // Arrange
            InitializeEmailCampaignTrackingService<ActivitySummaryReport>();
            string campaignId = "123456";

            // Act
            await emailCampaignTrackingService.GetActivitySummaryReportAsync(campaignId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<ActivitySummaryReport>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetBounceActivityReportAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<BounceActivityReport>>();
            string campaignId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<BounceActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<BounceActivityReport>());

            // Act
            await emailCampaignTrackingService.GetBounceActivityReportAsync(campaignId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/tracking/bounces?api_key={2}&limit=500", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetBounceActivityReportAsync_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<BounceActivityReport>>();
            string campaignId = "123456";

            // Act
            await emailCampaignTrackingService.GetBounceActivityReportAsync(campaignId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<BounceActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetClickActivityReportAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<EmailClickActivityReport>>();
            string campaignId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<EmailClickActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<EmailClickActivityReport>());

            // Act
            await emailCampaignTrackingService.GetClickActivityReportAsync(campaignId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/tracking/clicks?api_key={2}&limit=500", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetClickActivityReportAsync_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<EmailClickActivityReport>>();
            string campaignId = "123456";

            // Act
            await emailCampaignTrackingService.GetClickActivityReportAsync(campaignId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<EmailClickActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************               

        [Fact]
        public async Task GetClickByLinkActivityReportAsync_UpdateSummary_False_Uses_Correct_Endpoint()
        {            
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<ClickByLinkActivityReport>>();
            string campaignId = "123456";
            string linkId = "123";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<ClickByLinkActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<ClickByLinkActivityReport>());

            // Act
            await emailCampaignTrackingService.GetClickByLinkActivityReportAsync(campaignId, linkId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/tracking/clicks/{2}?api_key={3}", settings.BaseUrl, campaignId, linkId, settings.ApiKey), endpoint.AbsoluteUri);              
        }

        [Fact]
        public async Task GetClickByLinkActivityReportAsync_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<ClickByLinkActivityReport>>();
            string campaignId = "123456";
            string linkId = "123";

            // Act
            await emailCampaignTrackingService.GetClickByLinkActivityReportAsync(campaignId, linkId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<ClickByLinkActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************       

        [Fact]
        public async Task GetForwardActivityReportAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<ForwardActivityReport>>();
            string campaignId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<ForwardActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<ForwardActivityReport>());

            // Act
            await emailCampaignTrackingService.GetForwardActivityReportAsync(campaignId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/tracking/forwards?api_key={2}&limit=500", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetForwardActivityReportAsync_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<ForwardActivityReport>>();
            string campaignId = "123456";

            // Act
            await emailCampaignTrackingService.GetForwardActivityReportAsync(campaignId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<ForwardActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************       

        [Fact]
        public async Task GetOpenActivityReportAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<OpenActivityReport>>();
            string campaignId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<OpenActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<OpenActivityReport>());

            // Act
            await emailCampaignTrackingService.GetOpenActivityReportAsync(campaignId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/tracking/opens?api_key={2}&limit=500", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetOpenActivityReportAsync_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<OpenActivityReport>>();
            string campaignId = "123456";

            // Act
            await emailCampaignTrackingService.GetOpenActivityReportAsync(campaignId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<OpenActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetSendActivityReportAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<SendActivityReport>>();
            string campaignId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<SendActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<SendActivityReport>());

            // Act
            await emailCampaignTrackingService.GetSendActivityReportAsync(campaignId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/tracking/sends?api_key={2}&limit=500", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetSendActivityReportAsync_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<SendActivityReport>>();
            string campaignId = "123456";

            // Act
            await emailCampaignTrackingService.GetSendActivityReportAsync(campaignId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<SendActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************      

        [Fact]
        public async Task GetUnsubscribeActivityReport_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<UnsubscribeActivityReport>>();
            string campaignId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<UnsubscribeActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<UnsubscribeActivityReport>());

            // Act
            await emailCampaignTrackingService.GetUnsubscribeActivityReportAsync(campaignId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/tracking/unsubscribes?api_key={2}&limit=500", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetUnsubscribeActivityReportAsync_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignTrackingService<PaginatedResult<UnsubscribeActivityReport>>();
            string campaignId = "123456";

            // Act
            await emailCampaignTrackingService.GetUnsubscribeActivityReportAsync(campaignId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<UnsubscribeActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }
    }
}