﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.EventSpotEvents;
using SteadyContactSdk.EventSpotEvents.Validation;
using SteadyContactSdk.Helpers;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace SteadyContactTests.EventSpotEventTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class PromoCodeValidatorTests
    {
        private readonly PromoCodeValidator<PromoCode> validator;

        public PromoCodeValidatorTests()
        {
            validator = new PromoCodeValidator<PromoCode>();
        }

        // *************************************************************************************************

        [Fact]
        public void CodeName_IsNullOrWhiteSpace_Generates_Error()
        {
            // Arrange
            var codeName = "";
            var promoCode = new DiscountAmountPromoCode { CodeName = codeName };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "CodeName is required"));
        }

        [Fact]
        public void CodeName_Is_3_Characters_Generates_Error()
        {
            // Arrange
            var codeName = "ABC";
            var promoCode = new DiscountAmountPromoCode { CodeName = codeName };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "CodeName must be between 4 and 12 characters and cannot contain spaces or special characters"));
        }

        [Fact]
        public void CodeName_Is_13_Characters_Generates_Error()
        {
            // Arrange
            var codeName = "ABCDEFGHIJKLMN";
            var promoCode = new DiscountAmountPromoCode { CodeName = codeName };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "CodeName must be between 4 and 12 characters and cannot contain spaces or special characters"));
        }

        [Fact]
        public void CodeName_Contains_Space_Generates_Error()
        {
            // Arrange
            var codeName = "ABCDEF GHIJKL";
            var promoCode = new DiscountAmountPromoCode { CodeName = codeName };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "CodeName must be between 4 and 12 characters and cannot contain spaces or special characters"));
        }

        [Fact]
        public void CodeName_Contains_Special_Character_Generates_Error()
        {
            // Arrange
            var codeName = "ABCDEF_GHIJKL";
            var promoCode = new DiscountAmountPromoCode { CodeName = codeName };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "CodeName must be between 4 and 12 characters and cannot contain spaces or special characters"));
        }

        [Fact]
        public void CodeName_Is_Not_NullOrWhiteSpace_And_Between_4_And_12_Alphanumeric_Characters_Is_Valid()
        {
            // Arrange
            var codeName = "ABCDE123";
            var promoCode = new DiscountAmountPromoCode { CodeName = codeName };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "CodeName must be between 4 and 12 characters and cannot contain spaces or special characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void FeeIds_Has_Zero_Elements_When_CodeType_Is_ACCESS_Generates_Error()
        {
            // Arrange
            var feeIds = new List<string>();
            var promoCode = new DiscountAmountPromoCode { FeeIds = feeIds };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "FeeIds must contain only one fee id when CodeType is ACCESS"));
        }

        [Fact]
        public void FeeIds_Has_Two_Elements_When_CodeType_Is_ACCESS_Generates_Error()
        {
            // Arrange
            var feeIds = new List<string> { "", "" };
            var promoCode = new DiscountAmountPromoCode { FeeIds = feeIds };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "FeeIds must contain only one fee id when CodeType is ACCESS"));
        }

        [Fact]
        public void FeeIds_Has_One_Element_When_CodeType_Is_ACCESS_Is_Valid()
        {
            // Arrange
            var feeIds = new List<string>() { "" };
            var promoCode = new DiscountAmountPromoCode { FeeIds = feeIds };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "FeeIds must contain only one fee id when CodeType is ACCESS"));
        }

        // *************************************************************************************************

        [Fact]
        public void DiscountScope_Is_NONE_When_CodeType_Is_DISCOUNT_Generates_Error()
        {
            // Arrange
            var discountScope = PromoCodeDiscountScope.None;
            var codeType = PromoCodeType.Discount;
            var promoCode = new DiscountAmountPromoCode { CodeType = codeType, DiscountScope = discountScope };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "DiscountScope is required when CodeType is DISCOUNT"));
        }

        [Fact]
        public void DiscountScope_Is_FEE_LIST_When_CodeType_Is_DISCOUNT_Is_Valid()
        {
            // Arrange
            var discountScope = PromoCodeDiscountScope.FeeList;
            var codeType = PromoCodeType.Discount;
            var promoCode = new DiscountAmountPromoCode { CodeType = codeType, DiscountScope = discountScope };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "DiscountScope is required when CodeType is DISCOUNT"));
        }

        [Fact]
        public void DiscountScope_Is_ORDER_TOTAL_When_CodeType_Is_DISCOUNT_Is_Valid()
        {
            // Arrange
            var discountScope = PromoCodeDiscountScope.OrderTotal;
            var codeType = PromoCodeType.Discount;
            var promoCode = new DiscountAmountPromoCode { CodeType = codeType, DiscountScope = discountScope };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "DiscountScope is required when CodeType is DISCOUNT"));
        }

        // *************************************************************************************************

        [Fact]
        public void QuantityTotal_Is_Zero_Generates_Error()
        {
            // Arrange
            var quantityTotal = 0;
            var promoCode = new DiscountAmountPromoCode { QuantityTotal = quantityTotal };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "QuantityTotal must be a positive number or -1, where -1 is unlimited"));
        }

        [Fact]
        public void QuantityTotal_Is_Negitive_Two_Generates_Error()
        {
            // Arrange
            var quantityTotal = -2;
            var promoCode = new DiscountAmountPromoCode { QuantityTotal = quantityTotal };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "QuantityTotal must be a positive number or -1, where -1 is unlimited"));
        }

        [Fact]
        public void QuantityTotal_Is_Negitive_One_Is_Valid()
        {
            // Arrange
            var quantityTotal = -1;
            var promoCode = new DiscountAmountPromoCode { QuantityTotal = quantityTotal };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "QuantityTotal must be a positive number or -1, where -1 is unlimited"));
        }

        [Fact]
        public void QuantityTotal_Is_One_Is_Valid()
        {
            // Arrange
            var quantityTotal = 1;
            var promoCode = new DiscountAmountPromoCode { QuantityTotal = quantityTotal };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "QuantityTotal must be a positive number or -1, where -1 is unlimited"));
        }

        // *************************************************************************************************

        [Fact]
        public void DiscountAmount_Is_One_When_CodeType_Is_Discount_And_DiscountPercent_Is_Valid()
        {
            // Arrange
            var discountAmount = 1;
            var codeType = PromoCodeType.Discount;
            var promoCode = new DiscountAmountPromoCode { CodeType = codeType, DiscountAmount = discountAmount };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "DiscountAmount must be greater than 0.00"));
        }

        // *************************************************************************************************

        [Fact]
        public void DiscountPercent_Is_One_When_CodeType_Is_Discount_And_DiscountAmount_Is_Valid()
        {
            // Arrange
            var discountPercent = 1;
            var codeType = PromoCodeType.Discount;
            var promoCode = new DiscountPercentPromoCode { CodeType = codeType, DiscountPercent = discountPercent };

            // Act
            var result = validator.Validate(promoCode);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "DiscountPercent must be between 1 and 100"));
        }
    }
}