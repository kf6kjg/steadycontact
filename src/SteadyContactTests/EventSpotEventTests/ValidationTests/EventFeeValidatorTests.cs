﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.EventSpotEvents;
using SteadyContactSdk.EventSpotEvents.Validation;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace SteadyContactTests.EventSpotEventTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class EventFeeValidatorTests
    {
        private readonly EventFeeValidator validator;

        public EventFeeValidatorTests()
        {
            validator = new EventFeeValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void EarlyFee_Is_Negitive_Generates_Error()
        {
            // Arrange
            var earlyFee = -1;
            var model = new EventFee { EarlyFee = earlyFee };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EarlyFee must be $0 or greater"));
        }

        [Fact]
        public void EarlyFee_Is_Zero_Is_Valid()
        {
            // Arrange
            var earlyFee = 0;
            var model = new EventFee { EarlyFee = earlyFee };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EarlyFee must be $0 or greater"));
        }

        [Fact]
        public void EarlyFee_Is_Greater_Than_Zero_Is_Valid()
        {
            // Arrange
            var earlyFee = 50;
            var model = new EventFee { EarlyFee = earlyFee };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EarlyFee must be $0 or greater"));
        }

        // *************************************************************************************************

        [Fact]
        public void Fee_Is_Negitive_Generates_Errors()
        {
            // Arrange
            var fee = -1;
            var model = new EventFee { Fee = fee };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Fee must be greater than $0"));
        }

        [Fact]
        public void Fee_Is_Zero_Generates_Errors()
        {
            // Arrange
            var fee = 0;
            var model = new EventFee { Fee = fee };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Fee must be greater than $0"));
        }

        [Fact]
        public void Fee_Is_Greater_Than_Zero_Is_Valid()
        {
            // Arrange
            var fee = 50;
            var model = new EventFee { Fee = fee };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Fee must be greater than $0"));
        }

        // *************************************************************************************************

        [Fact]
        public void Label_IsNullOrWhiteSpace_Generates_Error()
        {
            // Arrange
            var label = "";
            var model = new EventFee { Label = label };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Label is required"));
        }

        [Fact]
        public void Label_Is_Not_NullOrWhiteSpace_Is_Valid()
        {
            // Arrange
            var label = "Fee";
            var model = new EventFee { Label = label };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Label is required"));
        }

        // *************************************************************************************************

        [Fact]
        public void LateFee_Is_Negitive_Generates_Error()
        {
            // Arrange
            var lateFee = -1;
            var model = new EventFee { LateFee = lateFee };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "LateFee must be $0 or greater"));
        }

        [Fact]
        public void LateFee_Is_Zero_Is_Valid()
        {
            // Arrange
            var lateFee = 0;
            var model = new EventFee { LateFee = lateFee };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "LateFee must be $0 or greater"));
        }

        [Fact]
        public void LateFee_Is_Greater_Than_Zero_Is_Valid()
        {
            // Arrange
            var lateFee = 50;
            var model = new EventFee { LateFee = lateFee };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "LateFee must be $0 or greater"));
        }
    }
}