﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.EventSpotEvents;
using SteadyContactSdk.EventSpotEvents.Validation;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace SteadyContactTests.EventSpotEventTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class TrackInformationValidatorTests
    {
        private readonly TrackInformationValidator validator;

        public TrackInformationValidatorTests()
        {
            validator = new TrackInformationValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void Early_Fee_Date_Is_Invalid_Iso8601_Date_Generates_Error()
        {
            // Arrange
            var earlyFeeDate = new DateTime(1969, 12, 31).ToString("o");
            var model = new TrackInformation { EarlyFeeDate = earlyFeeDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EarlyFeeDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        [Fact]
        public void Early_Fee_Date_Is_Valid_Iso8601_Date_Is_Valid()
        {
            // Arrange
            var earlyFeeDate = new DateTime(1970, 1, 1).ToString("o");
            var model = new TrackInformation { EarlyFeeDate = earlyFeeDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EarlyFeeDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        // *************************************************************************************************

        [Fact]
        public void Guest_Display_Label_Longer_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var guestDisplayLabel = "kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk";
            var model = new TrackInformation { GuestDisplayLabel = guestDisplayLabel };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "GuestDisplayLable cannot be greater than 50 characters"));

        }

        [Fact]
        public void Guest_Display_Less_Longer_Than_50_Characters_Is_Valid()
        {
            // Arrange
            var guestDisplayLabel = "Dr.";
            var model = new TrackInformation { GuestDisplayLabel = guestDisplayLabel };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "GuestDisplayLable cannot be greater than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Guest_Limit_Is_0_Characters_Is_Valid()
        {
            // Arrange
            var guestLimit = 0;
            var model = new TrackInformation { GuestLimit = guestLimit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "GuestLimit cannot exceed 100 guests"));
        }

        [Fact]
        public void Guest_Limit_Greater_Than_100_Generates_Error()
        {
            // Arrange
            var guestLimit = 101;
            var model = new TrackInformation { GuestLimit = guestLimit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "GuestLimit cannot exceed 100 guests"));
        }

        [Fact]
        public void Guest_Limit_Less_Than_100_Is_Valid()
        {
            // Arrange
            var guestLimit = 50;
            var model = new TrackInformation { GuestLimit = guestLimit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "GuestLimit cannot exceed 100 guests"));
        }

        // *************************************************************************************************

        [Fact]
        public void Is_Guest_Name_Required_Is_True_Is_Guest_Anonymous_Enabled_Is_True_Generates_Error()
        {
            // Arrange
            var isGuestNameRequried = true;
            var isGuestAnonymousEnabled = true;
            var model = new TrackInformation { IsGuestNameRequired = isGuestNameRequried, IsGuestAnonymousEnabled = isGuestAnonymousEnabled };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "IsGuestNameRequired must be set to false when IsGuestAnonymousEnabled is set to true"));
        }

        [Fact]
        public void Is_Guest_Name_Required_Is_False_Is_Guest_Anonymous_Enabled_Is_True_Is_Valid()
        {
            // Arrange
            var isGuestNameRequried = false;
            var isGuestAnonymousEnabled = true;
            var model = new TrackInformation { IsGuestNameRequired = isGuestNameRequried, IsGuestAnonymousEnabled = isGuestAnonymousEnabled };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "IsGuestNameRequired must be set to false when IsGuestAnonymousEnabled is set to true"));
        }

        [Fact]
        public void Is_Guest_Name_Required_Is_True_Is_Guest_Anonymous_Enabled_Is_False_Is_Valid()
        {
            // Arrange
            var isGuestNameRequried = true;
            var isGuestAnonymousEnabled = false;
            var model = new TrackInformation { IsGuestNameRequired = isGuestNameRequried, IsGuestAnonymousEnabled = isGuestAnonymousEnabled };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "IsGuestNameRequired must be set to false when IsGuestAnonymousEnabled is set to true"));
        }

        [Fact]
        public void Is_Guest_Name_Required_Is_False_Is_Guest_Anonymous_Enabled_Is_False_Is_Valid()
        {
            // Arrange
            var isGuestNameRequried = false;
            var isGuestAnonymousEnabled = false;
            var model = new TrackInformation { IsGuestNameRequired = isGuestNameRequried, IsGuestAnonymousEnabled = isGuestAnonymousEnabled };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "IsGuestNameRequired must be set to false when IsGuestAnonymousEnabled is set to true"));
        }

        // *************************************************************************************************

        [Fact]
        public void Late_Fee_Date_Is_Invalid_Iso8601_Date_Generates_Error()
        {
            // Arrange
            var lateFeeDate = new DateTime(1969, 12, 31).ToString("o");
            var model = new TrackInformation { LateFeeDate = lateFeeDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "LateFeeDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        [Fact]
        public void Late_Fee_Date_Is_Valid_Iso8601_Date_Is_Valid()
        {
            // Arrange
            var lateFeeDate = new DateTime(1970, 1, 1).ToString("o");
            var model = new TrackInformation { LateFeeDate = lateFeeDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "LateFeeDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        // *************************************************************************************************

        [Fact]
        public void Registration_Limit_Count_Is_Negitive_1_Generates_Error()
        {
            // Arrange
            var registrationLimitCount = -1;
            var model = new TrackInformation { RegistrationLimitCount = registrationLimitCount };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "RegistrationLimitCount must be a positive number"));
        }

        [Fact]
        public void Registration_Limit_Count_Is_0_Is_Valid()
        {
            // Arrange
            var registrationLimitCount = 0;
            var model = new TrackInformation { RegistrationLimitCount = registrationLimitCount };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "RegistrationLimitCount must be a positive number"));
        }

        [Fact]
        public void Registration_Limit_Count_Is_1_Is_Valid()
        {
            // Arrange
            var registrationLimitCount = 1;
            var model = new TrackInformation { RegistrationLimitCount = registrationLimitCount };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "RegistrationLimitCount must be a positive number"));
        }

        [Fact]
        public void Registration_Limit_Date_Is_Invalid_Iso8601_Date_Generates_Error()
        {
            // Arrange
            var registrationLimitDate = new DateTime(1969, 12, 31).ToString("o");
            var model = new TrackInformation { RegistrationLimitDate = registrationLimitDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "RegistrationLimitDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        [Fact]
        public void Registration_Limit_Date_Is_Valid_Iso8601_Date_Is_Valid()
        {
            // Arrange
            var registrationLimitDate = new DateTime(1970, 1, 1).ToString("o");
            var model = new TrackInformation { RegistrationLimitDate = registrationLimitDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "RegistrationLimitDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }
    }
}