﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.EventSpotEvents;
using SteadyContactSdk.EventSpotEvents.Validation;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace SteadyContactTests.EventSpotEventTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class EventSpotContactValidatorTests
    {
        private readonly EventSpotContactValidator validator;

        public EventSpotContactValidatorTests()
        {
            validator = new EventSpotContactValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void Email_Address_Longer_Than_128_Charaters_Generates_Error()
        {
            // Arrange
            var emailAddress = "scooper@caltech.edukkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk";
            var model = new EventSpotContact { EmailAddress = emailAddress };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EmailAddress cannot be greater than 128 characters"));
        }

        [Fact]
        public void Email_Address_Less_Than_128_Charaters_Is_Valid()
        {
            // Arrange
            var emailAddress = "scooper@caltech.edu";
            var model = new EventSpotContact { EmailAddress = emailAddress };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EmailAddress cannot be greater than 128 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Name_IsNullOrWhiteSpace_Generates_Error()
        {
            // Arrange
            var name = "";
            var model = new EventSpotContact { Name = name };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Name is required"));
        }

        [Fact]
        public void Name_Longer_Than_100_Charaters_Generates_Error()
        {
            // Arrange
            var name = "Sheldon Cooperkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk";
            var model = new EventSpotContact { Name = name };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Name cannot be greater than 100 characters"));
        }

        [Fact]
        public void Name_Less_Than_100_Charaters_Is_Valid()
        {
            // Arrange
            var name = "Sheldon Cooper";
            var model = new EventSpotContact { Name = name };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Name cannot be greater than 100 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Organization_Name_Longer_Than_255_Charaters_Generates_Error()
        {
            // Arrange
            var organizationName = "kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk";
            var model = new EventSpotContact { OrganizationName = organizationName };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "OrganizationName cannot be greater than 255 characters"));
        }

        [Fact]
        public void Organization_Name_Less_Than_255_Charaters_Is_Valid()
        {
            // Arrange
            var organizationName = "California Institute of Technology";
            var model = new EventSpotContact { OrganizationName = organizationName };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "OrganizationName cannot be greater than 255 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Phone_Number_Longer_Than_25_Charaters_Generates_Error()
        {
            // Arrange
            var phoneNumber = "(626) 395-6811 x1234567890";
            var model = new EventSpotContact { PhoneNumber = phoneNumber };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PhoneNumber cannot be greater than 25 characters"));
        }

        [Fact]
        public void Phone_Number_Less_Than_25_Charaters_Is_Valid()
        {
            // Arrange
            var phoneNumber = "(626) 395-6811";
            var model = new EventSpotContact { PhoneNumber = phoneNumber };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "PhoneNumber cannot be greater than 25 characters"));
        }
    }
}