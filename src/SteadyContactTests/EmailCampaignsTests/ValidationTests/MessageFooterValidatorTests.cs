﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.EmailCampaigns;
using SteadyContactSdk.EmailCampaigns.Validation;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace SteadyContactTests.EmailCampaignsTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class MessageFooterValidatorTests
    {
        private readonly MessageFooterValidator validator;

        public MessageFooterValidatorTests()
        {
            validator = new MessageFooterValidator();
        }

        // *************************************************************************************************    

        [Fact]
        public void AddressLine1_Is_Empty_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter()
            {
                AddressLine1 = ""
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "AddressLine2 must not exceed 50 characters"));
        }

        [Fact]
        public void AddressLine1_Length_Greater_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var footer = new MessageFooter
            {
                AddressLine1 = "Address 1 is way super longer than an impossible 50 characters"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "AddressLine1 must not exceed 50 characters"));
        }

        [Fact]
        public void AddressLine1_Length_Less_Than_Or_Eqaul_To_50_Characters_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                AddressLine1 = "Address 1 is less than 50 characters"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "AddressLine1 must not exceed 50 characters"));
        }

        // *************************************************************************************************    

        [Fact]
        public void AddressLine2_Is_Empty_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter()
            {
                AddressLine2 = ""
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "AddressLine2 must not exceed 50 characters"));
        }

        [Fact]
        public void AddressLine2_Length_Greater_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var footer = new MessageFooter
            {
                AddressLine2 = "Address 2 is way super longer than an impossible 50 characters"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "AddressLine2 must not exceed 50 characters"));
        }

        [Fact]
        public void AddressLine2_Length_Less_Than_Or_Eqaul_To_50_Characters_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                AddressLine2 = "Address 2 is less than 50 characters"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "AddressLine2 must not exceed 50 characters"));
        }

        // *************************************************************************************************    

        [Fact]
        public void AddressLine3_Is_Empty_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter()
            {
                AddressLine3 = ""
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "AddressLine3 must not exceed 50 characters"));
        }

        [Fact]
        public void AddressLine3_Length_Greater_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var footer = new MessageFooter
            {
                AddressLine3 = "Address 3 is way super longer than an impossible 50 characters"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "AddressLine3 must not exceed 50 characters"));
        }

        [Fact]
        public void AddressLine3_Length_Less_Than_Or_Eqaul_To_50_Characters_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                AddressLine3 = "Address 3 is less than 50 characters"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "AddressLine3 must not exceed 50 characters"));
        }

        // *************************************************************************************************    

        [Fact]
        public void Country_Is_Empty_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                Country = ""
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("Country must not exceed 2 characters") || e.ErrorMessage.Equals("Country must be in all upper case") || e.ErrorMessage.Equals("Country must be a valid ISO 31660-1 Alpha 2 Country Code")));
        }

        [Fact]
        public void Country_Is_Iso_3166_1_Alpha_2_Code_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                Country = "US"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("Country must not exceed 2 characters") || e.ErrorMessage.Equals("Country must be in all upper case") || e.ErrorMessage.Equals("Country must be a valid ISO 31660-1 Alpha 2 Country Code")));
        }

        // *************************************************************************************************    

        [Fact]
        public void ForwardEmailLinkText_Is_Greater_Than_45_Characters_Generates_Errors()
        {
            // Arrange
            var footer = new MessageFooter
            {
                ForwardEmailLinkText = "Forward email link text is way longer than fourty-five characters"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Equals("ForwardEmailLinkText must not exceed 45 characters")));
        }

        [Fact]
        public void ForwardEmailLinkText_Is_Empty_When_IncludeForwardEmail_Is_True_Generates_Errors()
        {
            // Arrange
            var footer = new MessageFooter
            {
                ForwardEmailLinkText = "",
                IncludeForwardEmail = true
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Equals("ForwardEmailLinkText is required when IncludeForwardEmail is true")));
        }

        [Fact]
        public void ForwardEmailLinkText_Is_Empty_When_IncludeForwardEmail_Is_False_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                ForwardEmailLinkText = "",
                IncludeForwardEmail = false
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("ForwardEmailLinkText is required when IncludeForwardEmail is true")));
        }

        // *************************************************************************************************    

        [Fact]
        public void InternationalState_Is_Greater_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var footer = new MessageFooter
            {
                InternationalState = "International State is way longer than fifty characters"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Equals("InternationalState must not exceed 50 characters")));
        }

        [Fact]
        public void InternationalState_Is_Emtpty_When_Country_Is_Not_US_Generates_Error()
        {
            // Arrange
            var footer = new MessageFooter
            {
                Country = "AU",
                InternationalState = ""
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Equals("InternationalState is required if the Country is not US")));
        }

        [Fact]
        public void InternationalState_Is_Empty_When_Country_Is_US_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                Country = "US",
                InternationalState = ""
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("InternationalState is required if the Country is not US")));
        }

        [Fact]
        public void InternationalState_Is_Not_Empty_When_Country_Is_AU_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                Country = "AU",
                InternationalState = "Queensland"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("InternationalState is required if the Country is not US")));
        }

        // *************************************************************************************************    

        [Fact]
        public void OrganizationName_Is_Greater_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var footer = new MessageFooter
            {
                OrganizationName = "This organizations Name is way longer than fifty characters"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Equals("OrganizationName must not exceed 50 characters")));
        }

        [Fact]
        public void OrganizationName_Is_Empty_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                OrganizationName = ""
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("OrganizationalName must not exceed 50 characters")));
        }

        // *************************************************************************************************    

        [Fact]
        public void PostalCode_Is_Greater_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var footer = new MessageFooter
            {
                PostalCode = "This postal code is way longer than fifty characters and isn't even a postal code"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Equals("PostalCode must not exceed 25 characters")));
        }

        [Fact]
        public void PostalCode_Is_Empty_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                PostalCode = ""
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("PostalCode must not exceed 25 characters")));
        }

        // *************************************************************************************************    

        [Fact]
        public void State_Is_Greater_Than_Three_Characters_Generates_Error()
        {
            // Arrange
            var footer = new MessageFooter
            {
                State = "CALIFORNIA"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Equals("State must not exceed 2 characters")));
        }

        [Fact]
        public void State_Is_Empty_Generates_Errors_When_Country_Is_US()
        {
            // Arrange
            var footer = new MessageFooter
            {
                State = "",
                Country = "US"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Equals("State is required when Country is US")));
        }

        [Fact]
        public void State_Is_Empty_Is_Valid_When_Country_Is_Not_US()
        {
            // Arrange
            var footer = new MessageFooter
            {
                State = "",
                Country = "CA"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("State is required when Country is US")));
        }

        [Fact]
        public void State_Is_Not_Empty_Is_Valid_When_Country_Is_US()
        {
            // Arrange
            var footer = new MessageFooter
            {
                State = "CA",
                Country = "US"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("State is required when Country is US")));
        }

        // *************************************************************************************************    

        [Fact]
        public void SubscribeLinkText_Is_Greater_Than_45_Characters_Generates_Error()
        {
            // Arrange
            var footer = new MessageFooter
            {
                SubscribeLinkText = "This subscribe link text is way longer than 45 characters"
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Equals("SubscribeLinkText must not exceed 45 characters")));
        }

        [Fact]
        public void SubscribeLinkText_Is_Empty_When_Include_Subscribe_Link_Is_True_Generates_Error()
        {
            // Arrange
            var footer = new MessageFooter
            {
                InlcudeSubscribeLink = true,
                SubscribeLinkText = "",
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Equals("SubscribeLinkText is required when IncludeSubscribeLink is true")));
        }

        [Fact]
        public void SubscribeLinkText_Is_Empty_When_Include_Subscribe_Link_Is_False_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                InlcudeSubscribeLink = false,
                SubscribeLinkText = "",
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("SubscribeLinkText is required when IncludeSubscribeLink is true")));
        }

        [Fact]
        public void SubscribeLinkText_Is_Not_Empty_When_Include_Subscribe_Link_Is_False_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                InlcudeSubscribeLink = false,
                SubscribeLinkText = "Subscribe link text",
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("SubscribeLinkText is required when IncludeSubscribeLink is true")));
        }

        [Fact]
        public void SubscribeLinkText_Is_Not_Empty_When_Include_Subscribe_Link_Is_True_Is_Valid()
        {
            // Arrange
            var footer = new MessageFooter
            {
                InlcudeSubscribeLink = true,
                SubscribeLinkText = "Subscribe link text",
            };

            // Act
            var result = validator.Validate(footer);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Equals("SubscribeLinkText is required when IncludeSubscribeLink is true")));
        }
    }
}