﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using FluentValidation.TestHelper;
using SteadyContactSdk.MyLibrary.Models;
using SteadyContactSdk.MyLibrary.Validation;
using Xunit;

namespace SteadyContactTests.MyLibraryTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class GetLibraryFileStatusModelValidatorTests
    {
        private readonly GetLibraryFileStatusModelValidator validator;

        public GetLibraryFileStatusModelValidatorTests()
        {
            validator = new GetLibraryFileStatusModelValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void FileIds_IfNull_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new GetLibraryFileStatusModel()
            {
                FileIds = null
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileIds, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileIds_IfEmpty_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new GetLibraryFileStatusModel()
            {
                FileIds = new List<string>()
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileIds, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileIds_IfContainsNullItems_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new GetLibraryFileStatusModel()
            {
                FileIds = new List<string>() { (string)null }
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileIds, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileIds_IfContainsEmptyItems_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new GetLibraryFileStatusModel()
            {
                FileIds = new List<string>() { "" }
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileIds, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileIds_IfContainsWhiteSpaceItems_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new GetLibraryFileStatusModel()
            {
                FileIds = new List<string>() { "   " }
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileIds, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileIds_IfContainsValidItems_PassesValidation()
        {
            // Arrange
            var success = true;
            var model = new GetLibraryFileStatusModel()
            {
                FileIds = new List<string>() { "2" }
            };

            // Act
            try
            {
                validator.ShouldNotHaveValidationErrorFor(x => x.FileIds, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileIds_IfInvalid_AddsError()
        {
            // Arrange
            var model = new GetLibraryFileStatusModel()
            {
                FileIds = null
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "You must specify a list of valid fileIds").Any());
        }

        // *************************************************************************************************

    }
}