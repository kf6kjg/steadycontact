﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Diagnostics.CodeAnalysis;
using System.Linq;
using FluentValidation.TestHelper;
using SteadyContactSdk.MyLibrary.Models;
using SteadyContactSdk.MyLibrary.Validation;
using Xunit;

namespace SteadyContactTests.MyLibraryTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class UpdateLibraryFolderModelValidatorTests
    {
        private readonly UpdateLibraryFolderModelValidator validator;

        public UpdateLibraryFolderModelValidatorTests()
        {
            validator = new UpdateLibraryFolderModelValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void FolderId_IfNull_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = null,
                Name = "foo",
                ParentId = "11"
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FolderId, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FolderId_IfEmpty_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "",
                Name = "foo",
                ParentId = "11"
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FolderId, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FolderId_IfWhiteSpace_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "   ",
                Name = "foo",
                ParentId = "11"
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FolderId, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FolderId_IfInvalid_AddsError()
        {
            // Arrange
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = null,
                Name = "foo",
                ParentId = "11"
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "You must specify the folder.").Any());
        }

        // *************************************************************************************************

        [Fact]
        public void Name_IfNull_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "12",
                Name = null,
                ParentId = "11"
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Name, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Name_IfEmpty_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "12",
                Name = "",
                ParentId = "11"
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Name, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Name_IfWhiteSpace_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "12",
                Name = "   ",
                ParentId = "11"
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Name, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Name_IfLengthIs40_PassesValidation()
        {
            // Arrange
            var success = true;
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "12",
                Name = string.Join("", Enumerable.Range(1, 40).Select(x => "a")),
                ParentId = "11"
            };

            // Act
            try
            {
                validator.ShouldNotHaveValidationErrorFor(x => x.Name, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Name_IfLengthIsGreaterThan40_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "12",
                Name = string.Join("", Enumerable.Range(1, 41).Select(x => "a")),
                ParentId = "11"
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Name, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Name_IfEmpty_AddsError()
        {
            // Arrange
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "12",
                Name = null,
                ParentId = "11"
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "You must specify a folder name.").Any());
        }

        [Fact]
        public void Name_IfTooLong_AddsError()
        {
            // Arrange
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "12",
                Name = string.Join("", Enumerable.Range(1, 41).Select(x => "a")),
                ParentId = "11"
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "The max folder name length is 40 characters.").Any());
        }

        // *************************************************************************************************

        [Fact]
        public void ParentId_IfNull_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "12",
                Name = "foo",
                ParentId = null
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.ParentId, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void ParentId_IfEmpty_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "12",
                Name = "foo",
                ParentId = ""
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.ParentId, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void ParentId_IfWhiteSpace_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "12",
                Name = "foo",
                ParentId = "   "
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.ParentId, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void ParentId_IfInvalid_AddsError()
        {
            // Arrange
            var model = new UpdateLibraryFolderModel()
            {
                FolderId = "12",
                Name = "foo",
                ParentId = null
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "You must specify the parent folder. To move a folder to level 1 in the directory structure, set to 0.").Any());
        }

        // *************************************************************************************************

    }
}