﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.Account;
using SteadyContactSdk.Account.Models;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Models;
using SteadyContactSdk.Validation;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Xunit;

namespace SteadyContactTests.AccountTests
{
    [ExcludeFromCodeCoverage]
    public class AccountServiceTests
    {
        private AccountService accountService = null;
        private ISteadyContactSettings settings = null;
        private Mock<IValidationService> validationService = null;
        private Mock<IWebServiceRequest> webServiceRequest = null;

        public AccountServiceTests()
        {
            settings = new SteadyContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            validationService = new Mock<IValidationService>();
            validationService.Setup(m => m.Validate(It.IsAny<IValidatable>()));

            webServiceRequest = new Mock<IWebServiceRequest>();
            webServiceRequest.Setup(m => m.GetDeserializedAsync<AccountSummary>(It.IsAny<Uri>(), It.IsAny<string>())).ReturnsAsync(new AccountSummary());
            webServiceRequest.Setup(m => m.PutDeserializedAsync<AccountSummary, AccountSummary>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<AccountSummary>())).ReturnsAsync(new AccountSummary());

            accountService = new AccountService(settings, validationService.Object, webServiceRequest.Object);
        }

        // *************************************************************************************************        

        [Fact]
        public async Task GetAccountInfoAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<AccountSummary>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new AccountSummary());

            // Act
            await accountService.GetAccountInfoAsync();

            // Assert
            Assert.Equal(string.Format("{0}account/info?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetAccountInfoAsync_Returns_Verify_WebServiceRequest_MakeRequest()
        {
            // Act
            await accountService.GetAccountInfoAsync();

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<AccountSummary>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************   

        [Fact]
        public async Task UpdateAccountInfoAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            var accountSummary = new AccountSummary();

            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<AccountSummary, AccountSummary>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<AccountSummary>()))
                .Callback((Uri a, string b, AccountSummary c) => endpoint = a)
                .ReturnsAsync(new AccountSummary());

            // Act
            await accountService.UpdateAccountInfoAsync(accountSummary);

            // Assert
            Assert.Equal(string.Format("{0}account/info?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateAccountInfoAsync_AccountSummary_IsValid_Verify_MakeRequest()
        {
            // Arrange
            var accountSummary = new AccountSummary();

            // Act
            await accountService.UpdateAccountInfoAsync(accountSummary);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<AccountSummary, AccountSummary>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<AccountSummary>()), Times.Once);
        }

        // *************************************************************************************************     

        [Fact]
        public async Task GetEmailAddressByStatus_Status_All_Uses_Correct_Endpoint()
        {
            // Arrange
            var status = EmailAddressStatus.All;

            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<AccountEmailAddress>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<AccountEmailAddress>());

            // Act
            await accountService.GetEmailAddressByStatusAsync(status);

            // Assert
            Assert.Equal(string.Format("{0}account/verifiedemailaddresses?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailAddressByStatus_Status_Confirmed_Uses_Correct_Endpoint()
        {
            // Arrange
            var status = EmailAddressStatus.Confirmed;

            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<AccountEmailAddress>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<AccountEmailAddress>());

            // Act
            await accountService.GetEmailAddressByStatusAsync(status);

            // Assert
            Assert.Equal(string.Format("{0}account/verifiedemailaddresses?api_key={1}&status=CONFIRMED", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailAddressByStatus_Status_Unconfirmed_Uses_Correct_Endpoint()
        {
            // Arrange
            var status = EmailAddressStatus.Unconfirmed;

            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<AccountEmailAddress>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<AccountEmailAddress>());

            // Act
            await accountService.GetEmailAddressByStatusAsync(status);

            // Assert
            Assert.Equal(string.Format("{0}account/verifiedemailaddresses?api_key={1}&status=UNCONFIRMED", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailAddressByStatus_Verify_MakeRequest()
        {
            // Arrange
            var status = EmailAddressStatus.All;

            // Act
            await accountService.GetEmailAddressByStatusAsync(status);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<IEnumerable<AccountEmailAddress>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************       

        [Fact]
        public async Task CreateEmailAddress_Uses_Correct_Endpoint()
        {
            // Arrange
            string email = "scooper@caltech.edu";

            // Act
            await accountService.CreateEmailAddressAsync(email);

            // Assert
        }

        [Fact]
        public async Task CreateEmailAddress_Verify_MakeRequest()
        {
            // Arrange
            string email = "scooper@caltech.edu";

            // Act
            await accountService.CreateEmailAddressAsync(email);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<List<EmailAddressModel>, IEnumerable<AccountEmailAddress>>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<List<EmailAddressModel>>()), Times.Once);
        }
    }
}