﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.Account;
using SteadyContactSdk.Account.Validation;
using SteadyContactSdk.Helpers;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace SteadyContactTests.AccountTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class AccountSummaryValidatorTests
    {
        private readonly AccountSummaryValidator validator;

        public AccountSummaryValidatorTests()
        {
            validator = new AccountSummaryValidator();
        }

        // *************************************************************************************************  

        [Fact]
        public void AccountSummary_Empty_AccountSummary_IsValid()
        {
            // Arrange
            var accountSummary = new AccountSummary();

            // Act
            var result = validator.Validate(accountSummary);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        // *************************************************************************************************  

        [Fact]
        public void Country_Code_Is_Two_Character_IsValid()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                CountryCode = CountryCode.US
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        // *************************************************************************************************

        [Fact]
        public void Email_Is_Invalid_Generates_Error()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                Email = "scooper@"
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert            
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Email is not a valid email address"));
        }

        [Fact]
        public void Email_Longer_Than_80_Characters_Generates_Error()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                Email = "scooper@wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.com"
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert            
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Email cannot contain more than 80 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void First_Name_Longer_Than_80_Characters_Generates_Error()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                FirstName = "sheldonwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "FirstName cannot contain more than 80 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Last_Name_Longer_Than_80_Characters_Generates_Error()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                LastName = "cooperwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "LastName cannot contain more than 80 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Organization_Address_Has_One_Element_Is_Valid()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                OrganizationAddresses = new OrganizationAddress[] 
                {
                    new OrganizationAddress()
                }
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        [Fact]
        public void Organization_Address_Has_More_Than_One_Element_Generates_Error()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                OrganizationAddresses = new OrganizationAddress[] 
                {
                    new OrganizationAddress(),
                    new OrganizationAddress()
                }
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "organization_address can only contain one element"));
        }

        // *************************************************************************************************

        [Fact]
        public void Organization_Name_Longer_Than_80_Characters_Generates_Error()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                OrganizationName = "california institute of technology wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "OrganizationName cannot contain more than 80 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Phone_Longer_Than_20_Characters_Generates_Error()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                Phone = "(626) 395-6811 x00000"
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Phone cannot contain more than 20 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void State_Code_Is_One_Character_Generates_Error()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                StateCode = "C"
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "StateCode must be exactly 2 characters"));
        }

        [Fact]
        public void State_Code_Is_Two_Character_IsValid()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                CountryCode = CountryCode.CA
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        // *************************************************************************************************

        [Fact]
        public void Website_Longer_Than_255_Characters_Generates_Error()
        {
            // Arrange
            var accountSummary = new AccountSummary()
            {
                Website = "https://caltech.edu/wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
            };

            // Act
            var result = validator.Validate(accountSummary);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Website cannot contain more than 255 characters"));
        }
    }
}