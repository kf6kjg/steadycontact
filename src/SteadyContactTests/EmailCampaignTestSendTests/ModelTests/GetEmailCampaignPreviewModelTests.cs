﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Diagnostics.CodeAnalysis;
using SteadyContactSdk.EmailCampaignTestSend.Models;
using SteadyContactSdk.Helpers;
using SteadyContactSdk.Models;
using Xunit;

namespace SteadyContactTests.EmailCampaignTestSendTests.ModelTests
{
    [ExcludeFromCodeCoverage]
    public class GetEmailCampaignPreviewModelTests
    {
        private readonly ISerialization serialization;
        private readonly GetEmailCampaignPreviewModel model;

        public GetEmailCampaignPreviewModelTests()
        {
            serialization = new Serialization();
            model = new GetEmailCampaignPreviewModel();
        }

        // *************************************************************************************************

        [Fact]
        public void Serializes_DefaultObject()
        {
            // Act
            var result = serialization.Serialize(model);

            // Assert
            Assert.Equal("{}", result);
        }

        [Fact]
        public void Serializes_CompleteObject()
        {
            // Arrange
            model.CampaignId = "aaa";
            model.FromEmail = "from@example.com";
            model.PreviewHtmlContent = "preview-html";
            model.PreviewTextContent = "preview-text";
            model.ReplyToEmail = "reply@example.com";
            model.Subject = "subject";

            // Act
            var result = serialization.Serialize(model);

            // Assert
            Assert.Equal("{\"from_email\":\"from@example.com\",\"preview_html_content\":\"preview-html\",\"preview_text_content\":\"preview-text\",\"reply_to_email\":\"reply@example.com\",\"subject\":\"subject\"}", result);
        }

        // *************************************************************************************************

        [Fact]
        public void GenerateEndpoint_CreatesCorrectEndpoint()
        {
            // Arrange
            ISteadyContactSettings settings = new SteadyContactSettings("key", "token", "http://");

            model.CampaignId = "aaa";

            // Act
            var result = model.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(string.Format("{0}{1}/{2}/preview?api_key={3}", "http://", "emailmarketing/campaigns", model.CampaignId, "key"), result);
        }

        // *************************************************************************************************

    }
}