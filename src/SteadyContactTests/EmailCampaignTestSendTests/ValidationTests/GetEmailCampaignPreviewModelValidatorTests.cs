﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Diagnostics.CodeAnalysis;
using System.Linq;
using SteadyContactSdk.EmailCampaignTestSend.Models;
using SteadyContactSdk.EmailCampaignTestSend.Validation;
using Xunit;

namespace SteadyContactTests.EmailCampaignTestSendTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class GetEmailCampaignPreviewModelValidatorTests
    {
        private readonly GetEmailCampaignPreviewModelValidator validator;

        public GetEmailCampaignPreviewModelValidatorTests()
        {
            validator = new GetEmailCampaignPreviewModelValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void CampaignId_IsNull_GeneratesError()
        {
            // Arrange
            var model = new GetEmailCampaignPreviewModel()
            {
                CampaignId = null
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.Equal(1, result.Errors.Count());
            Assert.Equal(1, result.Errors.Where(x => x.ErrorMessage == "CampaignId is required").Count());
        }

        [Fact]
        public void CampaignId_IsEmpty_GeneratesError()
        {
            // Arrange
            var model = new GetEmailCampaignPreviewModel()
            {
                CampaignId = ""
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.Equal(1, result.Errors.Count());
            Assert.Equal(1, result.Errors.Where(x => x.ErrorMessage == "CampaignId is required").Count());
        }

        [Fact]
        public void CampaignId_IsWhiteSpace_GeneratesError()
        {
            // Arrange
            var model = new GetEmailCampaignPreviewModel()
            {
                CampaignId = "    "
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.Equal(1, result.Errors.Count());
            Assert.Equal(1, result.Errors.Where(x => x.ErrorMessage == "CampaignId is required").Count());
        }
    }
}