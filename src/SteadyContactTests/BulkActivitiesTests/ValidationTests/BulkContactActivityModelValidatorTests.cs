﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.BulkActivities.Models;
using SteadyContactSdk.BulkActivities.Validation;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace SteadyContactTests.BulkActivitiesTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class BulkContactActivityModelValidatorTests
    {
        private readonly BulkContactActivityModelValidator validator;

        public BulkContactActivityModelValidatorTests()
        {
            validator = new BulkContactActivityModelValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void FileName_Is_NullOrWhiteSpace_Generates_Errors()
        {
            // Arrange
            var fileName = "";
            var data = new byte[0];
            var lists = "";
            var mimeType = "";

            var model = new BulkContactActivityModel { FileName = fileName, Data = data, Lists = lists, MimeType = mimeType };

            // Act
            var results = validator.Validate(model);

            // Assert
            Assert.True(results.Errors.Any(e => e.ErrorMessage == "FileName is requried"));
        }

        [Fact]
        public void FileName_Is_Not_NullOrWhiteSpace_Is_Valid()
        {
            // Arrange
            var fileName = "testfile.csv";
            var data = new byte[0];
            var lists = "";
            var mimeType = "";

            var model = new BulkContactActivityModel { FileName = fileName, Data = data, Lists = lists, MimeType = mimeType };

            // Act
            var results = validator.Validate(model);

            // Assert
            Assert.False(results.Errors.Any(e => e.ErrorMessage == "FileName is requried"));
        }

        // *************************************************************************************************

        [Fact]
        public void Data_Is_Null_Is_Generates_Errors()
        {
            // Arrange
            var fileName = "";
            byte[] data = null;
            var lists = "";
            var mimeType = "";

            var model = new BulkContactActivityModel { FileName = fileName, Data = data, Lists = lists, MimeType = mimeType };

            // Act
            var results = validator.Validate(model);

            // Assert
            Assert.True(results.Errors.Any(e => e.ErrorMessage == "The provided file was not able to be read"));
        }

        [Fact]
        public void Data_Is_Not_Null_Is_Valid()
        {
            // Arrange
            var fileName = "";
            var data = new byte[0];
            var lists = "";
            var mimeType = "";

            var model = new BulkContactActivityModel { FileName = fileName, Data = data, Lists = lists, MimeType = mimeType };

            // Act
            var results = validator.Validate(model);

            // Assert
            Assert.False(results.Errors.Any(e => e.ErrorMessage == "The provided file was not able to be read"));
        }

        // *************************************************************************************************

        [Fact]
        public void Lists_Not_Formated_Correctly_Generates_Errors()
        {
            // Arrange
            var fileName = "";
            var data = new byte[0];
            var lists = "NOT VALID";
            var mimeType = "";

            var model = new BulkContactActivityModel { FileName = fileName, Data = data, Lists = lists, MimeType = mimeType };

            // Act
            var results = validator.Validate(model);

            // Assert
            Assert.True(results.Errors.Any(e => e.ErrorMessage == "Lists must be a comma delimited set of contact list ids"));
        }

        [Fact]
        public void Lists_Is_Formated_Correctly_Is_Valid()
        {
            // Arrange
            var fileName = "";
            var data = new byte[0];
            var lists = "123456, 654321, 654123";
            var mimeType = "";

            var model = new BulkContactActivityModel { FileName = fileName, Data = data, Lists = lists, MimeType = mimeType };

            // Act
            var results = validator.Validate(model);

            // Assert
            Assert.False(results.Errors.Any(e => e.ErrorMessage == "Lists must be a comma delimited set of contact list ids"));
        }

        // *************************************************************************************************

        [Fact]
        public void MimeType_Is_NullOrWhiteSpace_Generates_Errors()
        {
            // Arrange
            var fileName = "";
            var data = new byte[0];
            var lists = "";
            var mimeType = "";

            var model = new BulkContactActivityModel { FileName = fileName, Data = data, Lists = lists, MimeType = mimeType };

            // Act
            var results = validator.Validate(model);

            // Assert
            Assert.True(results.Errors.Any(e => e.ErrorMessage == "MimeType is required"));
        }

        [Fact]
        public void MimeType_Is_Not_NullOrWhiteSpace_Is_Valid()
        {
            // Arrange
            var fileName = "";
            var data = new byte[0];
            var lists = "";
            var mimeType = "text/csv";

            var model = new BulkContactActivityModel { FileName = fileName, Data = data, Lists = lists, MimeType = mimeType };

            // Act
            var results = validator.Validate(model);

            // Assert
            Assert.False(results.Errors.Any(e => e.ErrorMessage == "MimeType is required"));
        }
    }
}