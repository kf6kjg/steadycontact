﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using SteadyContactSdk.BulkActivities;
using SteadyContactSdk.BulkActivities.Validation;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace SteadyContactTests.BulkActivitiesTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class ContactRemoveValidatorTests
    {
        private readonly ContactRemoveValidator validator;

        public ContactRemoveValidatorTests()
        {
            validator = new ContactRemoveValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void Import_Data_Is_Empty_Generates_Error()
        {
            // Arrange
            var model = new ContactRemove { ImportData = new ImportData[0], Lists = new List<string>() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ImportData is required"));
        }

        // *************************************************************************************************

        [Fact]
        public void Lists_Is_Empty_Generates_Error()
        {
            // Arrange
            var model = new ContactRemove { Lists = new List<string>() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Lists must contain at least one list id"));
        }

        [Fact]
        public void Lists_Is_Not_List_Of_Integers_Generates_Error()
        {
            // Arrange
            var model = new ContactRemove { Lists = new List<string> { "NOTVALID", "IDS" } };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Lists must be a comma delimited set of contact list ids"));
        }

        [Fact]
        public void Lists_Is_List_Of_Integers_Is_Valid()
        {
            // Arrange
            var model = new ContactRemove { Lists = new List<string> { "123456", "654321" } };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Lists must be a comma delimited set of contact list ids"));
        }
    }
}